//
// Created by Physiix on 19/05/17.
//

#ifndef OPENGRAPHICS_LOG_H
#define OPENGRAPHICS_LOG_H

#include <memory>
#include "include.h"

namespace OGA {
    struct Logger {
        virtual void Info(const String &message) = 0;
        virtual void Error(const String &message) = 0;
        virtual void Warning(const String &message) = 0;
    };

    class ConsoleLog final : public Logger {
    public:
        //! Output an info message to the console.
        //! \param message Message to log.
        virtual void Info(const String &message) override;

        //! Output an error message to the console.
        //! \param message Message to log.
        virtual void Error(const String &message) override;

        //! Output a warning message to the console.
        //! \param message Message to log.
        virtual void Warning(const String &message) override;
    };

    class Log final {
    private:
        typedef unique_ptr<Logger> LoggerPtr;

        //! Store a pointer to the currently used logger.
        static LoggerPtr logger;
    public:
        Log() = default;
        ~Log() = default;

        //! Log an info message with the currently log method.
        //! \param message Message to log.
        static void Info(const String &message) {
            Log::logger->Info(message);
        }

        //! Log an error message with the currently log method.
        //! \param message Message to log.
        static void Error(const String &message) {
            Log::logger->Error(message);
        }

        //! Log a warning message with the currently log method.
        //! \param message Message to log.
        static void Warning(const String &message) {
            Log::logger->Warning(message);
        }

        //! Change the currently used logger.
        //! \tparam T Type of the logger to set.
        template<typename T, typename = typename std::enable_if<std::is_base_of<Logger, T>{}, T >>
        static void SetLogger() {
            Log::logger = std::make_unique<T>(T());
        }
    };
}


#endif //OPENGRAPHICS_LOG_H

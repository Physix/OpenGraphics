//
// Created by Physiix on 21/05/17.
//

#ifndef OPENGRAPHICS_CONTEXT_H_H
#define OPENGRAPHICS_CONTEXT_H_H

#include "maths/math.h"
#include "renderer/renderer.h"
#include "log.h"
#include "oga_data.h"
#include "window/windows.h"

namespace OGA {
    struct Context {
        //! Object managing everything Screen/Window related.
		static Window window;

        //! Information about the APIs and Platforms.
		static ApiInfo apiInfo;

        //! Framebuffer used when offscreen rendering is enabled.
		static Framebuffer mainFramebuffer;

        //! Information about the application.
		static ApplicationInfo applicationInfo;

        //! Pointer to the current renderer.
		static unique_ptr<IRenderer> renderer;

        //! Whether the Context has been initialized or not.
		static bool initialized;

        Context() = default;
        Context(const Context &) = delete;
        Context &operator=(const Context &) = delete;
    };
}

#endif //OPENGRAPHICS_CONTEXT_H_H

//
// Created by Physiix on 19/05/17.
//

#ifndef OPENGRAPHICS_PLATFORM_H
#define OPENGRAPHICS_PLATFORM_H

#include "oga.h"

namespace OGA {
    //! Get the name of the current Operating System (platform).
    //! \return String with the name of the OS.
    static String GetPlatformName() {
		switch (Context::apiInfo.platform) {
            case Platform::WINDOWS:
                return "WINDOWS";
            case Platform::LINUX:
                return "LINUX";
            case Platform::OSX:
                return "OSX";
            case Platform::ANDROID:
                return "ANDROID";
            case Platform::IOS:
                return "IOS";
            case Platform::UNIX:
                return "UNIX";
            case Platform::DEFAULT:
            default:
                return "ERROR";
        }
    }

    //! Setup the platform information withing a context.
    static void SetupPlatform() {
#ifdef _WIN32
#define OGA_WINDOWS_32
        Context::apiInfo.platform = Platform::WINDOWS;
#ifdef _WIN64
#define OGA_WINDOWS_64
#else
#endif
#elif __APPLE__
#include "TargetConditionals.h"
#if TARGET_OS_IPHONE
#define OGA_IOS
        Context::apiInfo.platform = Platform::IOS;
#elif TARGET_OS_MAC
#define OGA_OSX
        Context::apiInfo.platform = Platform::OSX;
#else
#   error "Unknown Apple platform"
#endif
#elif __linux__
#define OGA_LINUX
		Context::apiInfo.platform = Platform::LINUX;
#elif __unix__
#define OGA_UNIX
        Context().Instance().apiInfo.platform = Platform::UNIX;
#else
#   error "Unknown compiler"
#endif
        Log::Warning("Setting up for platform " + GetPlatformName());
    }

    //! Execute a system on the currently used OS.
    //! \param command Command to execute.
    //! \return Result of the command.
    static int ExecuteSystemCommand(const char *command) {
        return std::system(command);
    }

}

#endif //OPENGRAPHICS_PLATFORM_H

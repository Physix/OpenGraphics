#ifndef OPENGRAPHICS_SHADER_DATA_H
#define OPENGRAPHICS_SHADER_DATA_H

#include "../include.h"

namespace OGA {
	enum class Pipeline {
		VERTEX, TESSELATION_EVALUATION, TESSELATION_CONTROL, GEOMETRY, FRAGMENT, COMPUTE
	};

	enum class ShaderType {
		GLSL, HLSL, VULKAN_GLSL, SPIR_V, DEFAULT
	};

	enum class FileType {
		BINARY, RAW
	};

	struct ShaderData {
		/// Absolute path to the shader file.
		String path{};

		/// Type of the shader file: Bin or raw.
		FileType type = FileType::RAW;

		/// Name of the shader to produce. The name is extracted from the path if not provided.
		String name{};

		/// Contains the source of the shader.
		String source{};

		/// Check if the shader has already been read.
		/// \return
		inline bool IsRead() const { return !ShaderData::source.empty(); }
	};

	struct ShaderInfo {
		/// Data about the shader to compile.
		ShaderData data;

		/// Language the input is written in.
		ShaderType input = ShaderType::DEFAULT;

		/// Language of the output we want.
		ShaderType output = ShaderType::DEFAULT;

		/// Output the debug info.
		bool verbose = false;

		/// Version of the output shader.
		uint32_t outputVersion = 0;
	};
}

#endif //OPENGRAPHICS_SHADER_DATA_H

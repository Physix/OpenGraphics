//
// Created by Physiix on 20/05/17.
//

#ifndef OPENGRAPHICS_SPIRVCOMPILER_H
#define OPENGRAPHICS_SPIRVCOMPILER_H

#include "../include.h"
#include "../platform.h"
#include "shader_data.h"
#include "transpiler.h"
#include <algorithm>
#include <fstream>
#include <sstream>

namespace OGA {
	/// Write the \typedef String \param content into a file.
	/// \param path Path of the file to write into.
	/// \param content Content to write.
	static void WriteToFile(const String &path, const String &content) {
		std::ofstream vOutput(path, std::ios_base::trunc);
		vOutput << content;
		vOutput.close();
	}

	struct CompilationResult {
		/// Whether the compilation has been succesfull or not.
		bool success = false;

		/// Contains the source of the compiled shader (if there is one).
		String source;

		CompilationResult() = default;

		CompilationResult(bool success)
			: success(success) {}

		CompilationResult(bool success, const String &source)
			: success(success), source(source) {}
	};

	//! Extract material data using a key.
	//! In the material files, the shaders are separated in blocks by keys like "#Vertex".
	//! This function extract all the content of each block.
	//! \param content Content to extract the data from.
	//! \param key Key to determine the block to extract.
	//! \return String containing the extracted block.
	static String ExtractMaterialContent(const String &content, const String &key) {
		String output;
		std::stringstream stream(content);
		// Flag set to true when the extraction has began.
		bool extracting = false;

		for (String line; std::getline(stream, line);) {
			// Storing all the data inside the key.
			if (extracting) {
				if (line == "#End")
					break;
				output += line + "\n";
			}

			// Start extracting the data.
			if (line == "#" + key)
				extracting = true;
		}

		return output;
	}

	//! Extract a value from the material using a key.
	//! \param content All the content of the material.
	//! \param key Key of the content to extract
	//! \return Value as a String.
	static String ExtractMaterialValue(const String &content, const String &key) {
		std::stringstream stream(content);
		for (String line; std::getline(stream, line);)
			if (line.find(key) != String::npos)
				return line.substr(line.find(key) + 6);
		return "";
	}

	//! Get shader type by its name.
	//! This function check if the name matches with any shader type.
	//! \param name Name of the required shader type.
	//! \return ShaderType requested if found, default if not.
	static ShaderType GetShaderType(const String &name) {
		if (name == "glsl")
			return ShaderType::GLSL;
		else if (name == "hlsl")
			return ShaderType::HLSL;
		else if (name == "vglsl")
			return ShaderType::VULKAN_GLSL;

		return ShaderType::DEFAULT;
	}

	//! Clean the content of a shader from Material specific syntax.
	//! \param content Content to clean.
	//! \return Cleaned content.
	static String CleanShaderContent(const String &content) {
		String output;

		std::stringstream stream(content);
		for (String line; getline(stream, line);)
			if (line.find("#version") != String::npos)
				output += line + "\n";
			else if (!line.empty() && line[0] != '#')
				output += line + "\n";

		return output;
	}

	//! Extract the Shader Information from the material.
	//! \param shadersInfo
	//! \param content
	//! \param key
	static 	void ExtractShaderData(ShaderInfo &shadersInfo, const String &content, const String &key) {
		const auto vertexContent = ExtractMaterialContent(content, key);
		shadersInfo.input = GetShaderType(ExtractMaterialValue(vertexContent, "#Lang"));
		String extension = ".frag";
		if (key == "Vertex")
			extension = ".vert";
		shadersInfo.data.path = Context::applicationInfo.structureInfo.pathTmp + "material_tmp" + extension;

		//Default language is GLSL.
		if (shadersInfo.input == ShaderType::DEFAULT) shadersInfo.input = ShaderType::GLSL;

		auto cleanShaderContent = CleanShaderContent(vertexContent);
		shadersInfo.data.source = cleanShaderContent;
		WriteToFile(shadersInfo.data.path, cleanShaderContent);
	}

	//! Take a path to the material file and extract all the information for the compilation.
	//! \param materialPath Path of the material to parse.
	//! \return CompilationInfo about the Vertex and Fragment shaders.
	static std::array<ShaderInfo, 2> ParseMaterial(const String &materialPath) {
		std::array<ShaderInfo, 2> shadersInfo;

		const auto materialFolder = Context::applicationInfo.structureInfo.pathMaterial;
		const auto absolutePath = materialFolder + materialPath;

		// Reading the material raw data.
		std::ifstream file(absolutePath);
		String content;
		for (String line; getline(file, line);)
			content += line + "\n";
		file.close();

		// Common data
		shadersInfo[0].data.name = ExtractMaterialValue(content, "#Name");
		shadersInfo[1].data.name = shadersInfo[0].data.name;
		shadersInfo[0].data.name += ".vert";
		shadersInfo[1].data.name += ".frag";
		switch (Context::apiInfo.graphicsApi) {
			case GraphicsApi::OpenGL:
			case GraphicsApi::Vulkan:
				shadersInfo[0].output = ShaderType::GLSL;
				shadersInfo[1].output = ShaderType::GLSL;
				break;
			case GraphicsApi::Direct3D9:
			case GraphicsApi::Direct3D11:
			case GraphicsApi::Direct3D12:
				shadersInfo[0].output = ShaderType::HLSL;
				shadersInfo[1].output = ShaderType::HLSL;
				break;
			case GraphicsApi::DEFAULT:
				OGA_ASSERT(false, "ParseMaterial: ERROR GraphicsAPI is set to default.");
				break;
		}

		// Extracting vertex shader data.
		ExtractShaderData(shadersInfo[0], content, "Vertex");
		ExtractShaderData(shadersInfo[1], content, "Fragment");

		return shadersInfo;
	}

	class ShaderCompiler final {
	public:
		static inline String ExtractNameFromPath(const String &path) {
			return path.substr(path.find_last_of("/\\") + 1);
		}

		//! Compile a shader from ShaderType to another.
		//! This function supports raw data as well as binary data.
		//! \param info Contains the information about the compilation.
		static CompilationResult Compile(ShaderInfo &info) {
			// Make sure the input and output are different
			if (info.input == info.output) {
				switch (info.input) {
					case ShaderType::GLSL:
						// If the input is the same as output, just copy the content inside the appropriate folder.
						WriteToFile(Context::applicationInfo.structureInfo.pathGLSL + info.data.name, info.data.source);
						break;
					case ShaderType::HLSL:
						break;
					case ShaderType::VULKAN_GLSL:
						break;
					case ShaderType::SPIR_V:
						break;
					case ShaderType::DEFAULT:
						break;
				}
				return CompilationResult{ true, info.data.path };
			}

			CompilationResult result{ false };

			switch (info.data.type) {
				case FileType::RAW:
					// Display a warning message if the user is requesting a compilation from HLSL to GLSL
					if (info.input == ShaderType::HLSL && info.output == ShaderType::GLSL)
						Log::Warning(
								"Compilation from HLSL to GLSL isn't fully supported, and will probably output unusable code.");
					break;
				case FileType::BINARY:
					// This is to make sure the user has requested either GLSL bin or VULKAN GLSL bin. HLSL bin is not supported.
					switch (info.output) {
						case ShaderType::GLSL:
						case ShaderType::SPIR_V:
						case ShaderType::VULKAN_GLSL:
							break;
						default:
							OGA_ASSERT(false, "OGA only support binary compilation to spir-v GLSL or spir-v Vulkan");
					}
					break;
			}

			// Prepare the transpiler
			Transpiler transpiler;

			auto platformName = GetPlatformName();
			std::transform(platformName.begin(), platformName.end(), platformName.begin(), ::tolower);

			// Information about the file.
			const auto fileName = (info.data.name.empty()) ? ExtractNameFromPath(info.data.path) : info.data.name;
			const auto structureInfo = Context::applicationInfo.structureInfo;
			const auto spirvShaderFile = structureInfo.pathSpirV + fileName;

			if (info.data.type == FileType::RAW)
				// Convert GLSL or HLSL to Spir-V before recompiling back.
				// For production, only this binary can be used for compilation.
				if (info.input == ShaderType::HLSL || info.input == ShaderType::GLSL ||
						info.input == ShaderType::VULKAN_GLSL)
					transpiler.Begin(info).Input(info.input).Output(ShaderType::SPIR_V).Transpile();

			// Check if the compilation requested isn't for a Spir-V binary file.
			if (info.output != ShaderType::SPIR_V && info.output != ShaderType::DEFAULT) {
				// Compile the Spir-V binary back to the requested shader type.

				transpiler.Input(ShaderType::SPIR_V).Output(info.output).Transpile(info);
				result.success = true;
				result.source = info.data.source;
			}
			return result;
		}
	};
} // namespace OGA
#endif //OPENGRAPHICS_SPIRVCOMPILER_H

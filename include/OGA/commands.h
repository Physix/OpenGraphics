#ifndef OPENGRAPHICS_COMMANDS_H
#define OPENGRAPHICS_COMMANDS_H

#include "memory.h"
#include "renderer/primitives.h"

namespace OGA {
	struct VertexAttribute;

	class RendererCommand {
	private:
		static LinearAllocator<8192> allocator;

	public:
		enum Target {
			VertexBufer, IndexBuffer, ShaderProgram, Texture, Framebuffer, NONE
		};

		enum class Type {
			CreateCommand, UpdateCommand, ReleaseCommand, NONE
		};

		// Target of the command.
		Target target = Target::NONE;

		// Type of the operation to handle by the command.
		Type type = Type::NONE;

		// Primitive to execute the command for.
		Primitive primitive;

		/// Create a new RendererCommand
		/// \tparam Command Type of the command.
		/// \return The newly created command object.
		template<typename Command, typename = typename std::enable_if<std::is_base_of<RendererCommand, Command>::value>>
		static inline Command *Create() { return allocator.Allocate<Command>(); };

		/// Reset the memory cursor, should be called after each pass.
		static inline void ResetMemory() { allocator.Reset(); }
	};

	struct FramebufferCommand : public RendererCommand {
		enum class AttachmentType {
			COLOR, DEPTH, NONE
		};
		/// Texture to attach to the framebuffer.
		std::vector<OGA::Texture> textures;

		/// Attachment type of the Texture to attach.
		AttachmentType attachmentType = AttachmentType::NONE;

		FramebufferCommand() { target = Framebuffer; }
	};

	struct PipelineCommand : public RendererCommand {
		/// Some additional data needs to be attached to certain commands.
		/// This is a buffer to fill in those cases. (ex, creating a VertexBuffer)
		Memory memory;
	};

	struct VertexCommand final : public PipelineCommand {
		using Attributes = std::vector<VertexAttribute>;
		/// Store all the attributes to create/update the VertexBuffer with.
		Attributes attributes;

		VertexCommand() { target = VertexBufer; }
	};

	struct ShaderProgramCommand final : public RendererCommand {
		/// Contains the Vertex shader source code.
		String vertexSource;

		/// Contains the Fragment shader source code.
		String fragmentSource;

		ShaderProgramCommand() { target = ShaderProgram; }
	};

	struct TextureCommand final : public PipelineCommand {
		enum class TextureType {
			TEXTURE1D, TEXTURE2D, TEXTURE3D
		};
		uint16_t width = 0;
		uint16_t height = 0;
		uint16_t length = 0;
		TextureType textureType = TextureType::TEXTURE1D;

		TextureCommand() { target = Texture; }
	};

	struct BindTextureCommand final : public RendererCommand {
		/// Name of the uniform to bind the texture to.
		String uniformName;
	};
}

#endif //OPENGRAPHICS_COMMANDS_H

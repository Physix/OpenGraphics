//
// Created by Physiix on 19/05/17.
//

#ifndef OPENGRAPHICS_INCLUDE_H
#define OPENGRAPHICS_INCLUDE_H

#include <iostream>
#include <string>
#include <vector>
#include <memory>

#define OGA_INLINE inline

#define DEFAULT_WIDTH 800
#define DEFAULT_HEIGHT 600

namespace OGA {
    static const constexpr char endl = '\n';

    using std::string;
    using std::vector;

    using std::unique_ptr;
    using std::shared_ptr;

    typedef string String;
    typedef float f32;

#ifdef _WIN32
    using ssize_t = long;
#endif


	static void OGA_ASSERT(bool condition, const String &message) {
        if (!condition) {
            std::cerr << "Error : " << message << '\n';
			exit(-1);
        }
    }
}
#if __cplusplus < 201402L
namespace std {
#ifndef _WIN32
	template<typename T, typename ...Args>
	static std::unique_ptr<T> make_unique(Args &&... args) {
		return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
	};
#endif

}
#endif

#endif //OPENGRAPHICS_INCLUDE_H

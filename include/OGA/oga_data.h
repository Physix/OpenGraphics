//
// Created by Physiix on 20/05/17.
//

#ifndef OPENGRAPHICS_OGA_DATA_H
#define OPENGRAPHICS_OGA_DATA_H

#include "maths/math.h"

namespace OGA {
	enum class Platform {
		WINDOWS, LINUX, OSX, ANDROID, IOS, UNIX, DEFAULT
	};

	enum class GraphicsApi {
		OpenGL, Vulkan, Direct3D9, Direct3D11, Direct3D12, DEFAULT
	};

	struct ApiInfo {
		/// Represent the current platform OS.
		Platform platform = Platform::DEFAULT;

		/// Graphics API to use.
		/// This is only taken in consideration at the start, changing it during
		/// runtime won't affect the rendering process.
		GraphicsApi graphicsApi = GraphicsApi::DEFAULT;
	};

	struct ScreenInfo {
		/// Flag set to true when the Screen color must be cleared after each frame.
		bool clearColor = true;

		/// Flag set to true when the depth must be cleared after each frame.
		bool clearDepth = true;

		/// Flag set to true when the V-Sync is enabled.
		bool vsync = false;

		/// Number of sampling to use for multi-sampling. Set value to 0 to disable multi-sampling.
		uint8_t samplingCount = 4;

		/// Background color to the screen with.
		Color background = Color(0.2f, 0.2f, 0.2f, 0.2f);

		/// Flag set to true if now window is to be spawned.
		bool offscreen = false;
	};

	class StructureInfo {
	public:
		/// Base path for the project.
		String resourcePath = String(OGA_PROJECT_DIRECTORY) + "/res/";

		/// Path to use when creating SpirV binary.
		String pathSpirV = StructureInfo::resourcePath + "spirv/";

		/// Path to use when create HLSL shaders.
		String pathHLSL = StructureInfo::resourcePath + "hlsl/";

		/// Path to use when create GLSL shaders.
		String pathGLSL = StructureInfo::resourcePath + "glsl/";

		/// Path storing all the materials files.
		String pathMaterial = StructureInfo::resourcePath + "material/";

		/// Temporary folder path.
		String pathTmp = StructureInfo::resourcePath + "tmp/";

	public:
		/// Set the name of the resource folder.
		/// This is a helper function which changes the resource folder name for all paths(eg. GLSL, HLSL, TMP etc.)
		/// \param name Name of the resources folder.
		void SetResourceName(const String &name) {
			StructureInfo::resourcePath = String(OGA_PROJECT_DIRECTORY) + "/" + name + "/";
			StructureInfo::pathSpirV = StructureInfo::resourcePath + "spirv/";
			StructureInfo::pathHLSL = StructureInfo::resourcePath + "hlsl/";
			StructureInfo::pathGLSL = StructureInfo::resourcePath + "glsl/";
			StructureInfo::pathMaterial = StructureInfo::resourcePath + "materials/";
			StructureInfo::pathTmp = StructureInfo::resourcePath + "tmp/";
		}
	};

	struct ApplicationInfo {
		/// Info about the application's screen.
		ScreenInfo screenInfo;

		/// Structure information about the application.
		StructureInfo structureInfo;

		/// Custom name that can be given and will be used with some Graphics Api (like Vulkan)
		String name;

		/// Current width of the window. This is not the same as the viewport's width.
		uint32_t width = DEFAULT_WIDTH;

		/// Current height of the window. This is not the same as the viewport's height.
		uint32_t height = DEFAULT_HEIGHT;
	};

}

#endif //OPENGRAPHICS_OGA_DATA_H

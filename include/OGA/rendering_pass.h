//
// Created by Physiix on 16/08/17.
//

#ifndef OPENGRAPHICS_PASS_H
#define OPENGRAPHICS_PASS_H

#include "renderer/renderer.h"

namespace OGA {
    /// Contains info about the current rendering pass.
    class RenderingPass {
    private:
        using Entries = std::vector<Entry>;

        /// Buffer containing all the entries to render for the next pass.
        static std::vector<Entry> entryBuffer;

    public:
        /// Current Entry.
        static Entry current;

        /// Push all the recorded entries to the renderer.
        static void Flush(Entries &entries) {
            std::move(entryBuffer.begin(), entryBuffer.end(), std::back_inserter(entries));
            RenderingPass::entryBuffer.erase(RenderingPass::entryBuffer.begin(), RenderingPass::entryBuffer.end());
        }

        /// Initialize the Entry to work with.
        /// \return Id of the last pushed Entry.
        static size_t InitEntry() {
            current = Entry{static_cast<uint32_t>(entryBuffer.size())};
            return current.id;
        }

        /// Submit a new entry for the next rendering pass.
        /// \param entry Entry to process.
        static void Push() {
            entryBuffer.push_back(std::move(current));
        }
    };


}
#endif //OPENGRAPHICS_PASS_H

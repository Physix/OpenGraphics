//
// Created by Physiix on 22/05/17.
//

#ifndef OPENGRAPHICS_OPENGL_RENDERER_H
#define OPENGRAPHICS_OPENGL_RENDERER_H

#include "../renderer.h"
#include <algorithm>
#include <unordered_map>

namespace OGA {
	struct OpenGLRenderer : public IRenderer {
	private:
		//! Texture held by the OpenGL Render for each OGA::Texture.
		struct GLTexture {
			enum Type {
				TEXTURE3D = GL_TEXTURE_3D,
				TEXTURE2D = GL_TEXTURE_2D,
				TEXTURE1D = GL_TEXTURE_1D
			};

			//! OGA Handle to the texture.
			Texture texture{};

			//! Type of the texture.
			//! 2D Texture by default.
			Type type = Type::TEXTURE2D;

			//! Immutable width of the Texture.
			uint32_t width = 1;

			//! Immutable height of the Texture.
			//! This variable is ignored when dealing with 1D textures.
			uint32_t height = 1;

			//! Immutable length of the Texture.
			//! This variable is ignored when dealing with 2D textures.
			uint32_t length = 1;

			GLTexture(const Texture &texture, Type type, uint32_t width)
				: texture(texture), type(type), width(width) {}

			GLTexture(const Texture &texture, Type type, uint32_t width, uint32_t height)
				: texture(texture),
				  type(type), width(width),
				  height(height) {}

			GLTexture(const Texture &texture, Type type, uint32_t width, uint32_t height, uint32_t length)
				: texture(
						  texture),
				  type(type), width(width), height(height), length(length) {}

			GLTexture() = default;
		};

		struct Viewport{
			/// X coordinate.
			uint16_t x;

			/// Y coordinate.
			uint16_t y;

			/// Width of the viewport.
			uint16_t width;

			/// Height of the viewport.
			uint16_t height;
		};

		struct GLFramebuffer {
			using ColorTextures = std::vector<Texture>;

			// Handle to the OGA framebuffer object.
			Framebuffer framebuffer;

			//! Contains all the attached color textures.
			ColorTextures colorTextures;

			// Handle to the depth texture (Or Invalid if not set).
			Texture DepthTexture;
		};

		typedef std::unordered_map<uint32_t, VertexBuffer> VertexMap;
		typedef std::unordered_map<uint32_t, std::pair<UniformType, String>> Uniforms;
		using Textures = std::vector<GLTexture>;
		using Framebuffers = std::vector<GLFramebuffer>;
		using PrimitiveIndex = decltype(std::declval<Primitive>().index);
		using PrimitiveMap = std::unordered_map<PrimitiveIndex, GLuint>; // OGA Index -> OpenGL Index

		//! Contains all the vertex buffers and their vertex array object.
		VertexMap vertexMap;

		//! Contains all the uniforms bound to the current context.
		Uniforms uniforms;

		//! Contains all the texture currently in used with the OpenGL Api.
		Textures textures;

		//! Contains all the framebuffers managed by the current GL Context.
		Framebuffers framebuffers;

		//! Map between indices of the OGA Library and OpenGL.
		PrimitiveMap primitives;

		/// Main viewport on screen.
		Viewport viewport;

	protected:
		void Notify() const override {
		}

		//! Check if any OpenGL error has been triggered.
		void CheckForErrors() const {
		}

		bool CompileSuccessful(uint32_t obj) {
			int status;
			glGetShaderiv(obj, GL_COMPILE_STATUS, &status);
			return status == GL_TRUE;
		}

		uint32_t CreateShader(const String &source, uint32_t type) {
			GLuint shader = glCreateShader(type);
			const char *glShader = source.c_str();
			glShaderSource(shader, 1, &glShader, NULL);
			glCompileShader(shader);
			if (static_cast<int>(CompileSuccessful(shader)) != GL_TRUE) {
				GLchar error[1024] = { 0 };
				glGetShaderInfoLog(shader, sizeof(error), NULL, error);
				Log::Error(String("Error : ") + error);
				exit(-1);
			}
			return shader;
		}

		void SetGlobalUniforms(ShaderProgram program) {
			this->SetUniformValue(globalUniforms.projection, &OpenGLRenderer::renderData.projectionMatrix.m11, program);
			this->SetUniformValue(globalUniforms.view, &OpenGLRenderer::renderData.projectionMatrix.m11, program);
		}

		/// Create a link between the OGA Primitive and the OpenGL primitive.
		/// \param primitiveIndex Index of the OGA Primitive.
		/// \param glIndex Index of the OpenGL Primitive.
		void LinkPrimitive(PrimitiveIndex primitiveIndex, GLuint glIndex) {
			OpenGLRenderer::primitives.insert({ primitiveIndex, glIndex });
		}

	public:
		void Init() override {
			globalUniforms.model = this->CreateUniform("m_Model", UniformType::Matrix4);
			globalUniforms.modelView = this->CreateUniform("m_ModelView", UniformType::Matrix4);
			globalUniforms.modelProjection = this->CreateUniform("m_ModelProjection", UniformType::Matrix4);
			globalUniforms.modelViewProjection = this->CreateUniform("m_ModelViewProjection", UniformType::Matrix4);
			globalUniforms.projection = this->CreateUniform("m_Projection", UniformType::Matrix4);
			globalUniforms.view = this->CreateUniform("m_View", UniformType::Matrix4);
			globalUniforms.viewProjection = this->CreateUniform("m_ViewProjection", UniformType::Matrix4);
		}

		virtual ArrayBuffer CreateArrayBuffer() override {
			ArrayBuffer arrayBuffer;
			return arrayBuffer;
		}

		void CreateVertexBuffer(void *data, uint32_t size, Attributes attributes, VertexBuffer vertexBuffer) override {
			uint32_t strideCount = 0;
			for (const auto &attr : attributes)
				strideCount += attr.count;

			// Generating and binding the Vertex Array Object
			uint32_t vao = 0;
			glGenVertexArrays(1, &vao);
			glBindVertexArray(vao);
			OpenGLRenderer::CheckForErrors();

			// Generating the vertex buffer and binding its data.
			uint32_t vbo = 0;
			glGenBuffers(1, &vbo);
			glBindBuffer(GL_ARRAY_BUFFER, vbo);
			glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
			OpenGLRenderer::CheckForErrors();

			// Setting the alignments of the VertexBuffer in the Shaders.
			uint32_t index = 0;
			uint32_t offset = 0;
			for (auto attribute : attributes) {
				GLuint type;
				switch (attribute.type) {
					case VertexAttribute::AttrType::Int:
						type = GL_INT;
						break;
					case VertexAttribute::AttrType::Short:
						type = GL_FLOAT;
						break;
					default:
						type = GL_FLOAT;
				}
				glEnableVertexAttribArray(index);
				glVertexAttribPointer(index, attribute.count, type, static_cast<GLboolean>(attribute.normalized),
						strideCount * sizeof(f32), (const void *)(offset * sizeof(f32)));
				offset += attribute.count;
				index++;
			}
			OpenGLRenderer::CheckForErrors();

			// Wrapping the vertex buffer and storing its container.
			OpenGLRenderer::primitives.insert({ vertexBuffer.index, vbo });
			OpenGLRenderer::vertexMap.insert({ vao, vertexBuffer });
		}

		uint32_t GetVerticesCount(uint32_t size, Attributes attributes) const override {
			size_t vertexSize = 0;
			for (const auto &attr : attributes) {
				switch (attr.type) {
					case VertexAttribute::AttrType::Float:
						vertexSize += sizeof(f32) * attr.count;
						break;
					case VertexAttribute::AttrType::Int:
						vertexSize += sizeof(int) * attr.count;
						break;
					case VertexAttribute::AttrType::Short:
						vertexSize += sizeof(uint8_t) * attr.count;
						break;
				}
			}
			return static_cast<uint32_t>(size / vertexSize);
		}

		void CreateIndexBuffer(void *data, uint32_t size, IndexBuffer indexBuffer) override {
			uint32_t ibo = 0;

			// Generating the index buffer and storing the data inside.
			glGenBuffers(1, &ibo);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);

			OpenGLRenderer::LinkPrimitive(indexBuffer.index, ibo);
		}

		uint32_t GetIndicesCount(void *data, uint32_t size) const override {
			return static_cast<uint32_t>(size / sizeof(uint32_t));
		}

		Uniform CreateUniform(const String &name, UniformType type) override {
			Uniform uniform;
			uniform.index = static_cast<uint32_t>(OpenGLRenderer::uniforms.size());
			OpenGLRenderer::uniforms.insert({ uniform.index, { type, name } });
			return uniform;
		}

		void SetUniformValue(Uniform uniform, const void *value, ShaderProgram program) override {
			OGA_ASSERT(OpenGLRenderer::uniforms.find(uniform.index) != OpenGLRenderer::uniforms.end(),
					"Cannot bind non created uniform.");

			auto lUniform = OpenGLRenderer::uniforms[uniform.index];
			GLuint programId = OpenGLRenderer::primitives[program.index];

			const auto location = glGetUniformLocation(programId, lUniform.second.c_str());
			switch (lUniform.first) {
				case UniformType::Int:
					glUniform1iv(location, 1, reinterpret_cast<const GLint *>(value));
					break;
				case UniformType::Float:
					glUniform1fv(location, 1, reinterpret_cast<const GLfloat *>(value));
					break;
				case UniformType::Vector4:
					glUniform4fv(location, 1, reinterpret_cast<const GLfloat *>(value));
					break;
				case UniformType::Matrix4:
					glUniformMatrix4fv(location, 1, GL_FALSE, reinterpret_cast<const GLfloat *>(value));
					break;
				case UniformType::Matrix3:
					glUniformMatrix3fv(location, 1, GL_FALSE, reinterpret_cast<const GLfloat *>(value));
					break;
			}
		}

		void CreateShaderProgram(const String &vertexSource, const String &fragmentSource,
				const ShaderProgram &program) override {
			GLuint programId = glCreateProgram();

			uint32_t vertexShader = CreateShader(vertexSource, GL_VERTEX_SHADER);
			uint32_t fragmentShader = CreateShader(fragmentSource, GL_FRAGMENT_SHADER);
			glAttachShader(programId, vertexShader);
			glAttachShader(programId, fragmentShader);

			glValidateProgram(programId);
			glLinkProgram(programId);

			OpenGLRenderer::LinkPrimitive(program.index, programId);
		}

		void CreateTexture1D(void *data, uint16_t width, Texture texture) override {
			GLuint textureId = 0;
			glGenTextures(1, &textureId);
			glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

			glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA8, width, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

			OpenGLRenderer::textures.push_back(GLTexture{ texture, GLTexture::Type::TEXTURE1D, width });
			OpenGLRenderer::LinkPrimitive(texture.index, textureId);
			free(data);
		}

		void CreateTexture2D(void *data, uint16_t width, uint16_t height, Texture texture) override {
			OpenGLRenderer::CheckErrors();
			GLuint textureId = 0;
			glGenTextures(1, &textureId);
			glBindTexture(GL_TEXTURE_2D, textureId);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

			glBindTexture(GL_TEXTURE_2D, 0);

			OpenGLRenderer::textures.push_back(GLTexture{ texture, GLTexture::Type::TEXTURE2D, width, height });
			OpenGLRenderer::LinkPrimitive(texture.index, textureId);
			free(data);
		}

		void CreateTexture3D(void *data, uint16_t width, uint16_t height, uint16_t length, Texture texture) override {
			GLuint textureId = 0;
			glGenTextures(1, &textureId);
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

			glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA8, width, height, length, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

			OpenGLRenderer::textures.push_back({ texture, GLTexture::Type::TEXTURE3D, width, height, length });
			OpenGLRenderer::LinkPrimitive(texture.index, textureId);
			free(data);
		}

		void *GetTextureData(Texture texture, void *data) override {
			// Check if the texture is registered within this GL Context.
			auto gTexture = std::find_if(textures.begin(), textures.end(), [texture](const GLTexture &t) {
				return t.texture.index == texture.index;
			});

			// Check if the texture has been found
			OGA_ASSERT(gTexture != OpenGLRenderer::textures.end(), "Trying to read data from unregistered texture");

			GLuint textureId = OpenGLRenderer::primitives[texture.index];

			// Query the texture data from OpenGL.
			//TODO: Add support for custom Format and Type.
			const auto format = GL_RGBA;
			const auto type = GL_UNSIGNED_BYTE;

			// If no container is provided, create a new one.
			const size_t size = gTexture->width * gTexture->height * gTexture->length * sizeof(uint8_t) * 4;
			if (data == nullptr)
				data = operator new(size);

			glPixelStorei(GL_PACK_ALIGNMENT, 4);
			glGetTextureImage(textureId, 0, format, type, static_cast<GLsizei>(size), data);
			return data;
		}

		void *ReadFramePixels(uint32_t x, uint32_t y, uint32_t width, uint32_t height) override {
			glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
			glPixelStorei(GL_PACK_ALIGNMENT, 4);

			auto data = reinterpret_cast<uint8_t *>(operator new(width *height * sizeof(uint8_t) * 4));
			glReadPixels(x, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);
			return data;
		}

		ivec3 GetTextureDimensions(Texture texture) override {
			// Check if the texture is initialized.
			if (texture.index == OGA_INVALID_ID)
				return {};

			// Check if the texture is registered.
			auto gTexture = std::find_if(textures.begin(), textures.end(), [texture](const GLTexture &t) {
				return t.texture.index == texture.index;
			});
			if (gTexture == OpenGLRenderer::textures.end())
				return {};

			// Store the data to retrieve.
			ivec3 dimensions;
			GLuint textureId = OpenGLRenderer::primitives.at(texture.index);
			glBindTexture(gTexture->type, textureId);
			glGetTexParameteriv(gTexture->type, GL_TEXTURE_WIDTH, &dimensions.x);
			glGetTexParameteriv(gTexture->type, GL_TEXTURE_HEIGHT, &dimensions.y);
			glGetTexParameteriv(gTexture->type, GL_TEXTURE_DEPTH, &dimensions.z);

			return dimensions;
		}

		void CreateFramebuffer(Framebuffer framebuffer) override {
			GLuint fbo = 0;
			glGenFramebuffers(1, &fbo);
			OpenGLRenderer::framebuffers.push_back({ framebuffer });
			OpenGLRenderer::LinkPrimitive(framebuffer.index, fbo);
		}

		void AttachColorTexture(Framebuffer framebuffer, Texture texture) override {
			OGA_ASSERT(framebuffer.index != OGA_INVALID_ID, "Framebuffer must be valid");
			OGA_ASSERT(texture.index != OGA_INVALID_ID, "Texture must be valid");

			GLuint fbo = OpenGLRenderer::primitives.at(framebuffer.index);
			GLuint textureId = OpenGLRenderer::primitives.at(texture.index);

			glBindFramebuffer(GL_FRAMEBUFFER, fbo);
			glBindTexture(GL_TEXTURE_2D, textureId);

			// Getting info about the texture.
			uint32_t width, height;
			auto glTexture = std::find_if(this->textures.begin(), this->textures.end(), [texture](const GLTexture &t) {
				return t.texture.index == texture.index;
			});
			OGA_ASSERT(glTexture != OpenGLRenderer::textures.end(),
					"Texture should be initialized before attaching it to a framebuffer.");
			width = glTexture->width;
			height = glTexture->height;

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glBindTexture(GL_TEXTURE_2D, 0);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);

			// Register the Color Texture with its framebuffer.
			auto gFramebuffer = std::find_if(framebuffers.begin(), framebuffers.end(),
					[framebuffer](const GLFramebuffer &f) {
						return framebuffer.index == f.framebuffer.index;
					});
			if (gFramebuffer != OpenGLRenderer::framebuffers.end())
				gFramebuffer->colorTextures.push_back(texture);

			glBindFramebuffer(GL_FRAMEBUFFER, 0); //Binding back to the screen.
		}

		void AttachColorTextures(Framebuffer framebuffer, vector<Texture> textures) override {
			OGA_ASSERT(framebuffer.index != OGA_INVALID_ID, "Framebuffer must be valid");
			OGA_ASSERT(textures.size() > 0, "Framebuffer requires at least one color texture to attach");

			GLuint fbo = OpenGLRenderer::primitives.at(framebuffer.index);
			glBindFramebuffer(GL_FRAMEBUFFER, fbo);

			uint32_t loopIndex = 0;

			// Store all the attachments to draw into.
			vector<uint32_t> attachments;

			// Iterating through each texture and setting it on its respective attachment.
			for (auto &texture : textures) {
				OGA_ASSERT(texture.index != OGA_INVALID_ID, "Texture must be valid");

				GLuint textureId = OpenGLRenderer::primitives.at(texture.index);
				glBindTexture(GL_TEXTURE_2D, textureId);

				// Getting info about the texture.
				uint32_t width, height;
				auto glTexture = std::find_if(this->textures.begin(), this->textures.end(),
						[texture](const GLTexture &t) {
							return t.texture.index == texture.index;
						});
				OGA_ASSERT(glTexture != OpenGLRenderer::textures.end(),
						"Texture should be initialized before attaching it to a framebuffer.");
				width = glTexture->width;
				height = glTexture->height;

				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

				glBindTexture(GL_TEXTURE_2D, 0);
				attachments.push_back(GL_COLOR_ATTACHMENT0 + loopIndex++);
				glFramebufferTexture2D(GL_FRAMEBUFFER, attachments[attachments.size() - 1], GL_TEXTURE_2D, textureId,
						0);
			}
			// Register the ATTACHMENT to draw into.
			glDrawBuffers(static_cast<GLsizei>(attachments.size()), attachments.data());

			glBindFramebuffer(GL_FRAMEBUFFER, 0); //Binding back to the screen.
		}

		void AttachDepthTexture(Framebuffer framebuffer, Texture texture) override {
			OGA_ASSERT(framebuffer.index != OGA_INVALID_ID, "Framebuffer must be valid");
			OGA_ASSERT(texture.index != OGA_INVALID_ID, "Texture must be valid");

			GLuint fbo = OpenGLRenderer::primitives.at(framebuffer.index);
			GLuint textureId = OpenGLRenderer::primitives.at(texture.index);

			glBindFramebuffer(GL_FRAMEBUFFER, fbo);
			glBindTexture(GL_TEXTURE_2D, textureId);

			// Getting info about the texture.
			uint32_t width, height;
			auto glTexture = std::find_if(this->textures.begin(), this->textures.end(), [texture](const GLTexture &t) {
				return t.texture.index == texture.index;
			});
			OGA_ASSERT(glTexture != OpenGLRenderer::textures.end(),
					"Texture should be initialized before attaching it to a framebuffer.");
			width = glTexture->width;
			height = glTexture->height;

			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL,
					GL_UNSIGNED_INT_24_8, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glBindTexture(GL_TEXTURE_2D, 0);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, textureId, 0);

			glBindFramebuffer(GL_FRAMEBUFFER, 0); //Binding back to the screen.
		}

		Texture GetColorTexture(Framebuffer framebuffer) override {
			if (framebuffer.index == OGA_INVALID_ID)
				return {};

			auto gFramebuffer = std::find_if(framebuffers.begin(), framebuffers.end(),
					[framebuffer](const GLFramebuffer &f) {
						return framebuffer.index == f.framebuffer.index;
					});

			if (gFramebuffer == OpenGLRenderer::framebuffers.end() || gFramebuffer->colorTextures.empty())
				return {};

			return gFramebuffer->colorTextures[0];
		}

		void ReleaseTexture(Texture &texture) override {
			GLuint textureId = OpenGLRenderer::primitives.at(texture.index);
			glDeleteTextures(1, &textureId);

			// Remove the texture the container.
			std::remove_if(textures.begin(), textures.end(), [texture](const GLTexture &t) {
				return t.texture.index == texture.index;
			});

			OpenGLRenderer::primitives.erase(texture.index);
			texture.index = OGA_INVALID_ID;
		}

		void ReleaseVertexBuffer(VertexBuffer &vertexBuffer) override {
			// Check if the vertex buffer has an associated vertex array.
			uint32_t vao = OGA_INVALID_ID;
			for (auto entry = OpenGLRenderer::vertexMap.begin(); entry != OpenGLRenderer::vertexMap.end();)
				if (entry->second.index == vertexBuffer.index) {
					vao = entry->first;

					// Delete the entry from the map if found.
					OpenGLRenderer::vertexMap.erase(entry);
					break;
				} else
					entry++;

			// Delete the vertex array object if found.
			if (vao != OGA_INVALID_ID)
				glDeleteVertexArrays(1, &vao);

			GLuint vbo = OpenGLRenderer::primitives.at(vertexBuffer.index);
			glDeleteBuffers(1, &vbo);
			vertexBuffer.index = OGA_INVALID_ID;
		}

		void ReleaseIndexBuffer(IndexBuffer &indexBuffer) override {
			GLuint indexId = OpenGLRenderer::primitives.at(indexBuffer.index);
			glDeleteBuffers(1, &indexId);

			OpenGLRenderer::primitives.erase(indexBuffer.index);
			indexBuffer.index = OGA_INVALID_ID;
		}

		void ReleaseFramebuffer(Framebuffer &framebuffer) override {
			glDeleteFramebuffers(1, &framebuffer.index);
		}

		void ReleaseShaderProgram(ShaderProgram &shaderProgram) override {
			glDeleteProgram(shaderProgram.index); //TODO: Release its shaders units.
		}

		void EnableBackCulling() override {
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
		}

		//! Check if any error has occurred during the last frame.
		void CheckErrors() const override {
			GLuint error;
			while ((error = glGetError()) != GL_NO_ERROR)
				Log::Error("OpenGL Error " + std::to_string(error));
		}

		/// Handle all the commands queued for the Render.
		/// This function, create, update and release primitives from the OpenGL API.
		/// THis function is executed once each pass.
		void HandleCommands() {
			for (auto command : OpenGLRenderer::commands) {
				switch (command->type) {
					case RendererCommand::Type::CreateCommand: {
						switch (command->target) {
							case RendererCommand::VertexBufer: {
								VertexBuffer vertexBuffer;
								auto vertexCommand = static_cast<VertexCommand *>(command);
								vertexBuffer.index = vertexCommand->primitive.index;
								OpenGLRenderer::CreateVertexBuffer(vertexCommand->memory.data,
										static_cast<uint32_t>(vertexCommand->memory.size),
										vertexCommand->attributes, vertexBuffer);
							} break;
							case RendererCommand::IndexBuffer: {
								IndexBuffer indexBuffer;
								auto indexCommand = static_cast<PipelineCommand *>(command);
								indexBuffer.index = indexCommand->primitive.index;
								OpenGLRenderer::CreateIndexBuffer(indexCommand->memory.data,
										static_cast<uint32_t>(indexCommand->memory.size),
										indexBuffer);
							} break;
							case RendererCommand::ShaderProgram: {
								ShaderProgram program;
								auto programCommand = static_cast<ShaderProgramCommand *>(command);
								program.index = programCommand->primitive.index;
								OpenGLRenderer::CreateShaderProgram(programCommand->vertexSource,
										programCommand->fragmentSource, program);
							} break;
							case RendererCommand::Texture: {
								Texture texture;
								auto textureCommand = static_cast<TextureCommand *>(command);
								texture.index = textureCommand->primitive.index;
								switch (textureCommand->textureType) {
									case TextureCommand::TextureType::TEXTURE1D:
										OpenGLRenderer::CreateTexture1D(textureCommand->memory.data,
												textureCommand->width, texture);
										break;
									case TextureCommand::TextureType::TEXTURE2D:
										OpenGLRenderer::CreateTexture2D(textureCommand->memory.data,
												textureCommand->width, textureCommand->height,
												texture);
										break;
									case TextureCommand::TextureType::TEXTURE3D:
										OpenGLRenderer::CreateTexture3D(textureCommand->memory.data,
												textureCommand->width, textureCommand->height,
												textureCommand->length, texture);
										break;
								}
								break;
							}
							case RendererCommand::Framebuffer: {
								Framebuffer framebuffer;
								framebuffer.index = command->primitive.index;
								OpenGLRenderer::CreateFramebuffer(framebuffer);
								break;
							}
							case RendererCommand::NONE:
								OGA_ASSERT(true, "`NONE` is not a valid command type.");
								break;
						}
						break;
					}
					case RendererCommand::Type::UpdateCommand: {
						switch (command->target) {
							case RendererCommand::VertexBufer:
								break;
							case RendererCommand::IndexBuffer:
								break;
							case RendererCommand::ShaderProgram:
								OGA_ASSERT(true, "Cannot update a shader program, something went wrong,");
								break;
							case RendererCommand::Texture:
								break;
							case RendererCommand::Framebuffer: {
								auto framebufferCommand = static_cast<FramebufferCommand *>(command);
								Framebuffer framebuffer;
								framebuffer.index = framebufferCommand->primitive.index;
								auto attachments = framebufferCommand->textures;
								switch (framebufferCommand->attachmentType) {
									case FramebufferCommand::AttachmentType::COLOR: {
										if (attachments.size() == 1)
											OpenGLRenderer::AttachColorTexture(framebuffer, attachments[0]);
										else
											OpenGLRenderer::AttachColorTextures(framebuffer, attachments);
										break;
									}
									case FramebufferCommand::AttachmentType::DEPTH:
										OpenGLRenderer::AttachDepthTexture(framebuffer, attachments[0]);
										break;
									case FramebufferCommand::AttachmentType::NONE:
										break;
								}
								break;
							}
							case RendererCommand::NONE:
								OGA_ASSERT(true, "`NONE` is not a valid command type.");
								break;
						}
						break;
					}
					case RendererCommand::Type::ReleaseCommand:
						switch (command->target) {
							case RendererCommand::VertexBufer: {
								VertexBuffer vertexBuffer;
								vertexBuffer.index = command->primitive.index;
								OpenGLRenderer::ReleaseVertexBuffer(vertexBuffer);
								break;
							}
							case RendererCommand::IndexBuffer: {
								IndexBuffer indexBuffer;
								indexBuffer.index = command->primitive.index;
								OpenGLRenderer::ReleaseIndexBuffer(indexBuffer);
								break;
							}
							case RendererCommand::ShaderProgram: {
								ShaderProgram program;
								program.index = command->primitive.index;
							} break;
							case RendererCommand::Texture: {
								Texture texture;
								texture.index = command->primitive.index;
								OpenGLRenderer::ReleaseTexture(texture);
								break;
							}
							case RendererCommand::Framebuffer: {
								Framebuffer framebuffer;
								framebuffer.index = command->primitive.index;
								break;
							}
							case RendererCommand::NONE:
								OGA_ASSERT(true, "Release command for `NONE` target invalid.");
								break;
						}
						break;
					case RendererCommand::Type::NONE:
						break;
				}
			}
			OpenGLRenderer::commands.clear();
		}

		using EntriesPtr = std::vector<Entry *>;
		using Renderables = std::unordered_map<uint32_t, EntriesPtr>; // Map ordered index => Vector of Entry pointers.

		void Render() override {
			OpenGLRenderer::UpdateAnalysisData();
			OpenGLRenderer::HandleCommands();

			// Sort the entries by framebuffer.
			Renderables renderables{ { { 0, {} } } }; // The first framebuffer is for onscreen rendering.
			uint32_t renderableIndex = 1; // Index of the framebuffer in the renderable map.
			for (const auto &framebuffer : OpenGLRenderer::framebuffers)
				renderables.insert({ renderableIndex++, {} });

			// Iterate through all the entries and sort them by their framebuffer.
			for (auto &entry : OpenGLRenderer::renderData.entries) {
				renderableIndex = 1;
				if (entry.framebuffer.index == OGA_INVALID_ID) // To render onscreen
					renderables[0].push_back(&entry);
				else
					// Offscreen rendering, determine to which framebuffer attach the entry.
					for (auto &glFramebuffer : OpenGLRenderer::framebuffers) {
						if (entry.framebuffer.index == glFramebuffer.framebuffer.index) {
							renderables[renderableIndex].push_back(&entry);
							break;
						}
						renderableIndex++;
					}
			}

			// Iterate through the renderable to render the entries to their appropriate framebuffer.
			for (const auto &renderable : renderables) {
				// Determine whether it's onscreen or offscreen rendering.
				const auto onscreen = renderable.first == 0;

				// Get the index of the glFramebuffer.
				const auto framebufferIndex = renderable.first - 1;

				// OpenGL Index for the framebuffer, invalid if onscreen.
				GLuint fbo = OGA_INVALID_ID;

				// Setup the framebuffer rendering stage.
				if (!onscreen) {
					// Reference to the Framebuffer data.
					const auto &glFramebuffer = OpenGLRenderer::framebuffers[framebufferIndex];

					// Get the OpenGL Index associated with the framebuffer.
					fbo = OpenGLRenderer::primitives.at(glFramebuffer.framebuffer.index);

					// Extract the the first color attachment, which will be used to set the viewport.
					if (!glFramebuffer.colorTextures.empty()) {
						const auto &texture = glFramebuffer.colorTextures[0];
						const auto glTexture = std::find_if(this->textures.begin(), this->textures.end(),
								[texture](const GLTexture &t) {
									return t.texture.index == texture.index;
								});

						// Bind the buffer and set the viewport value from the extracted texture.
						glBindFramebuffer(GL_FRAMEBUFFER, fbo);
						glViewport(0, 0, glTexture->width, glTexture->height);
						OpenGLRenderer::ClearScreen(OpenGLRenderer::backgroundColor);
					}
				}

				// Current viewport dimensions.
				glViewport(viewport.x, viewport.y, viewport.width, viewport.height);

				// Iterate trough the entries for the current Framebuffer and render them.
				for (const auto &entryPtr : renderable.second) {
					const auto &entry = *entryPtr;
					// Check if the VertexBuffer has been registered.
					GLuint vao = OGA_INVALID_ID;
					GLuint program = OpenGLRenderer::primitives.at(entry.program.index);
					GLuint vbo = OpenGLRenderer::primitives.at(entry.vertexBuffer.index);
					GLuint vio = OpenGLRenderer::primitives.at(entry.indexBuffer.index);

					for (auto &e : OpenGLRenderer::vertexMap)
						if (e.second.index == entry.vertexBuffer.index) {
							vao = e.first;
							break;
						}

					if (vao == OGA_INVALID_ID)
						continue;

					// Binding the VAO and program
					glUseProgram(program);
					glBindVertexArray(vao);
					glBindBuffer(GL_ARRAY_BUFFER, vbo);

					// Binding Entry uniform.
					OpenGLRenderer::SetGlobalUniforms(entry.program);
					for (const auto &entryUniform : entry.uniforms)
						SetUniformValue(entryUniform.first, entryUniform.second, entry.program);

					// Bind the textures
					GLenum textureIndex = 0;
					for (const auto &texture : entry.textures) {
						glActiveTexture(GL_TEXTURE0 + static_cast<GLenum>(textureIndex));
						glBindTexture(GL_TEXTURE_2D, static_cast<GLuint>(GetApiIndex(texture.second)));
						glUniform1i(glGetUniformLocation(program, texture.first.c_str()), textureIndex);
						textureIndex++;
					}

					// Drawing the geometry
					uint32_t mode;
					switch (entry.mode) {
						case DrawMode::POINTS:
							mode = GL_POINTS;
							break;
						case DrawMode::TRIANGLES:
							mode = GL_TRIANGLES;
							break;
						case DrawMode::TRIANGLE_STRIP:
							mode = GL_TRIANGLE_STRIP;
							break;
						case DrawMode::TRIANGLE_FAN:
							mode = GL_TRIANGLE_FAN;
							break;
					}

					if (entry.indexBuffer.index == OGA_INVALID_ID)
						glDrawArrays(mode, entry.vertexBuffer.count, 3);
					else {
						glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vio);
						glDrawElements(mode, entry.indexBuffer.count, GL_UNSIGNED_INT, 0);
					}
				}

				// Unbind the framebuffer if required.
				if (!onscreen || fbo != OGA_INVALID_ID)
					glBindFramebuffer(GL_FRAMEBUFFER, 0);
			}

			// Clear the entries for next frame.
			OpenGLRenderer::renderData.entries.clear();

			//Check for any errors.
			OpenGLRenderer::CheckErrors();

			// Reset the Commands memory
			RendererCommand::ResetMemory();
		}

		void Render(Framebuffer framebuffer) override {
			OpenGLRenderer::HandleCommands();
			GLuint fbo = OpenGLRenderer::primitives.at(framebuffer.index);
			glBindFramebuffer(GL_FRAMEBUFFER, fbo);
			OpenGLRenderer::ClearScreen(OpenGLRenderer::backgroundColor);
			this->Render();
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}

		size_t GetApiIndex(Primitive primitive) const override {
			if (primitive.index == OGA_INVALID_ID)
				return OGA_INVALID_ID;
			return OpenGLRenderer::primitives.at(primitive.index);
		}

		f32 GetElapsedTime() override {
			return analysis.elapsedTime;
		}

		uint16_t GetFrameRates() override {
			return static_cast<uint16_t>((1.f / analysis.elapsedTime));
		}

		void UpdateAnalysisData() {
			const f32 currentTime = static_cast<const f32>(glfwGetTime());
			// Calculating the elapsed time
			analysis.elapsedTime = currentTime - analysis.lastTime;
			analysis.lastTime = currentTime;
		}

		void ClearScreen(const Color &color) override {
			glClearColor(color.x, color.y, color.z, color.w);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}

		//! The logic of the function is handled by GLFW,
		//! since it managed the OpenGL Context.
		void SwapBuffers() override {
			glfwSwapBuffers(glfwGetCurrentContext());
		}

		//! Set the viewport resolution.
		//! \param x Coordinate from the left of the screen in the X Axis.
		//! \param y Coordinate from the bottom of the screen in the Y Axis.
		//! \param width Width of the GL Viewport.
		//! \param height Height of the GL Viewport.
		void SetViewport(uint16_t x, uint16_t y, uint16_t width, uint16_t height) override {
			OpenGLRenderer::viewport.x = x;
			OpenGLRenderer::viewport.y = y;
			OpenGLRenderer::viewport.width = width;
			OpenGLRenderer::viewport.height = height;
		}

		/// Execute the last registered commands (Most likely release commands)
		/// And shut down the renderer.
		void Shutdown() override {
			OpenGLRenderer::HandleCommands();
		}
	};
} // namespace OGA

#endif //OPENGRAPHICS_OPENGL_RENDERER_H

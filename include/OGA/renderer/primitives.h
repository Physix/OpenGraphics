//
// Created by Physiix on 22/05/17.
//

#ifndef OPENGRAPHICS_PRIMITIVES_H
#define OPENGRAPHICS_PRIMITIVES_H

#include <limits>
#include "../include.h"

#define OGA_PRIMITIVE(struct_name) \
    struct struct_name : public Primitive{};\

#define OGA_PRIMITIVE_COUNT(struct_name)\
    struct struct_name : public Primitive{uint32_t count;};\

namespace OGA {
    /// Create a *unique* Id for the primtive.
    /// TODO: Find a better way to handle the ID Generation.
    static uint32_t CreatePrimitiveID() {
        static uint32_t id = 0;
        return id++;
    }

    static const uint32_t OGA_INVALID_ID = std::numeric_limits<uint32_t>::max();

    struct Primitive {
        /// Index of the primitive.
        /// This index is only used for internal OGA mapping, it doesn't necessarily
        /// reflect the ID of the primitive in the selected Graphics API.
        /// For example, a primitive with an index of 1 may have its id 4 with OpenGL.
        uint32_t index = OGA_INVALID_ID;
    };

    OGA_PRIMITIVE(Framebuffer)
    OGA_PRIMITIVE(Texture)
    OGA_PRIMITIVE(ShaderProgram)
    OGA_PRIMITIVE(ArrayBuffer)
    OGA_PRIMITIVE(VertexContainer)
    OGA_PRIMITIVE(Uniform)
    OGA_PRIMITIVE_COUNT(VertexBuffer)
    OGA_PRIMITIVE_COUNT(IndexBuffer)


	struct VertexAttribute {
		enum class AttrType {
			Float, Int, Short
		};

		//! Number of component per vertex. Can be 1, 2, 3 or 4.
		uint8_t count;

		//! Type of the attribute to bind.
		AttrType type = AttrType::Float;

		//! Whether to normalize the value handled by the attribute or not.
		bool normalized = false;

		VertexAttribute(uint8_t count, AttrType type, bool normalized) : count(count), type(type),
																		 normalized(normalized) {}
	};

    struct TextureUniform{

    };

}


#endif //OPENGRAPHICS_PRIMITIVES_H

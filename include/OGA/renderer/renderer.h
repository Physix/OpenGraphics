//
// Created by Physiix on 19/05/17.
//

#ifndef OPENGRAPHICS_RENDERER_H_H
#define OPENGRAPHICS_RENDERER_H_H

#include "../include.h"
#include "../maths/math.h"
#include "primitives.h"
#include "../commands.h"
#include <functional>

namespace OGA {
    enum class DrawMode {
        POINTS,
        TRIANGLES,
        TRIANGLE_STRIP,
        TRIANGLE_FAN
    };
    enum class UniformType {
        Int,
        Float,
        Vector4,
        Matrix4,
        Matrix3
    };

    struct Entry final {
        //! Id of the Entry.
		uint32_t id = OGA_INVALID_ID;

        //! Store all the data about each vertex.
        //! It's up to the user to chose what's a `Vertex`.
		VertexBuffer vertexBuffer{};

        //! Store the indices of the vertices to draw.
        //! The indices should be the same order as the orientation (CW, CCW).
		IndexBuffer indexBuffer{};

        //! Some entries have additional array buffer with other data.
		vector<ArrayBuffer> arrayBuffers{};

        //! Store all the uniforms id and data.
		vector<std::pair<Uniform, const void *>> uniforms{};

		/// Contains all the textures to bind for the entry.
		vector<std::pair<String, Texture>> textures{};

        //! Shader program of the Entry.
        //! Literally the same size as uint16_t, so it doesn't matter if it gets copied.
		ShaderProgram program{};

        /// Hold the reference to the framebuffer this entry is rendered to. Default OGA_INVALID => onscreen.
		Framebuffer framebuffer{};

        //! Hold the matrix to send as Model to the shader.
        //! Must store the world geometry of the object.
		mat4 modelTransform{};

        //! The entry vertices will be drawn using what.
        DrawMode mode = DrawMode::TRIANGLES;

		Entry(uint32_t id) : id(id) {}
        Entry() = default;
        Entry(Entry &&) = default;
        Entry &operator=(Entry &&) = default;
    };

    struct RenderData final {
        //! Hold all the entries to draw in a single pass.
        vector<Entry> entries;

        //! View Matrix to apply in the shader for all the entries.
        mat4 viewMatrix;

        //! Projection Matrix to apply in the shader for all the entries.
        mat4 projectionMatrix;

        RenderData() = default;

        RenderData(RenderData &&) = default;

        //! Access to the last entry added.
        //! \return Reference to the last entry.
        inline Entry &Last() {
            OGA_ASSERT(RenderData::entries.size() > 0, "Error RenderData::Last empty entries.");
            return RenderData::entries[RenderData::entries.size() - 1];
        }
    };

    // Contains all the uniforms shared by all the programs.
    struct GlobalUniforms {
        Uniform projection;
        Uniform view;
        Uniform model;
        Uniform viewProjection;
        Uniform modelView;
        Uniform modelProjection;
        Uniform modelViewProjection;
    };

    struct AnalysisData{
    	/// Last time value taken.
    	f32 lastTime = 0.0f;

    	/// Elapsed time for the last rendering pass
    	f32 elapsedTime = 0.0f;

    	/// Estimated frame rates.
    	uint16_t frameRates = 0;
    };

    class IRenderer {
    public:
        // Helpers
        using Commands = std::vector<RendererCommand *>;

        /// Contains all the commands to be executed for the next
        Commands commands;

        //! Store all the data about the entries to render.
        RenderData renderData;

    protected:
        using Attributes = std::vector<VertexAttribute>;

        //! Contains all the global uniforms handles.
        GlobalUniforms globalUniforms;

        //! Send a notification that the rendering pass is finished.
        virtual void Notify() const = 0;

        //! Color to clear the background with.
        Color backgroundColor = vec4(0.2f, 0.2f, 0.2f, 1.0f);

        /// Analysis data for the renderer.
        AnalysisData analysis;

    public:
        IRenderer() = default;

        IRenderer(IRenderer &&) = default;

        virtual ~IRenderer() = default;

        //! Initialize the resources used by the renderer.
        virtual void Init() = 0;

        //! Create a new ArrayBuffer.
        //! \return Newly created ArrayBuffer.
        virtual ArrayBuffer CreateArrayBuffer() = 0;

        //! Create a new Vertex Buffer object.
        //! \param data Data to store inside the buffer.
        //! \param size Size of the buffer.
        //! \param attributes List of vertex attributes to bundle with the buffer.
        //! \return Newly created VertexBuffer
        virtual void
        CreateVertexBuffer(void *data, uint32_t size, Attributes attributes, VertexBuffer vertexBuffer) = 0;

        /// Get the vertices count from the size of the VertexBuffer and its attributes.
        //! \param size Size of the buffer.
        //! \param attributes List of vertex attributes to bundle with the buffer.
        /// \return Vertices count.
        virtual uint32_t GetVerticesCount(uint32_t size, Attributes attributes) const = 0;

        //! Create a buffer to store the indices of a mesh.
        //! \param data Contains all the indices to store.
        //! \param size Size of the buffer.
        //! \return New created IndexBuffer.
        virtual void CreateIndexBuffer(void *data, uint32_t size, IndexBuffer indexBuffer) = 0;

        /// Get the indices count from the data and size.
        //! \param data Contains all the indices to store.
        //! \param size Size of the buffer.
        /// \return Indices count.
        virtual uint32_t GetIndicesCount(void *data, uint32_t size) const = 0;

        //! Create a uniform that can be bind to a ShaderProgram.
        //! \param name Name of the uniform to create.
        //! \param type Type of uniform to create.
        //! \return The newly created uniform.
        virtual Uniform CreateUniform(const String &name, UniformType type) = 0;

        //! Push a new value to a uniform.
        //! \param uniform Uniform to push the value to.
        //! \param value New value of the uniform.
        virtual void SetUniformValue(Uniform uniform, const void *value, ShaderProgram program) =0;

        //! Create a Shader Program using the vertex and fragment sources.
        //! \param vertexSource Vertex shader source code in the language of the current Graphics API.
        //! \param fragmentSource Fragment shader source code in the language of the current Graphics API.
        //! \return Newly created ShaderProgram.
        virtual void CreateShaderProgram(const String &vertexSource, const String &fragmentSource,
                                         const ShaderProgram &shaderProgram) = 0;

        //! Create a new 1D Texture handle and populate with the data.
        //! If the data is null, the texture is initialized as black image.
        //! \param data Data to initialize the texture with.
        //! \param width Size of the texture in the X Axis.
        //! \return Newly create 1D Texture.
        virtual void CreateTexture1D(void *data, uint16_t width, Texture texture) = 0;

        //! Create a new 2D Texture handle and populate with the data.
        //! If the data is null, the texture is initialized as black image.
        //! \param data Data to initialize the texture with.
        //! \param width Size of the texture in the X Axis.
        //! \param height Size of the texture in the Y Axis.
        //! \return Newly create 2D Texture.
        virtual void CreateTexture2D(void *data, uint16_t width, uint16_t height, Texture texture) = 0;

        //! Create a new 3D Texture handle and populate with the data.
        //! If the data is null, the texture is initialized as black image.
        //! \param data Data to initialize the texture with.
        //! \param width Size of the texture in the X Axis.
        //! \param height Size of the texture in the Y Axis.
        //! \param length Size of the texture in the Z Axis.
        //! \return Newly create 3D Texture.
        virtual void CreateTexture3D(void *data, uint16_t width, uint16_t height, uint16_t length, Texture texture) = 0;

        //! Extract data from a texture with the Graphics Api.
        //! The data is then stored and pointer to it is returned.
        //! \param texture Texture the data from.
        //! \param data Container for the data of the texture. If null, a new container is created.
        //! \return Pointer to the texture data or nullptr.
        virtual void *GetTextureData(Texture texture, void *data) = 0;

        //! Read the screen Pixels from the current framebuffer.
        //! \param x X Value to start extracting pixels from.
        //! \param y Y Value to start extracting pixels from.
        //! \param width Width of the Texture to extract.
        //! \param height Height of the Texture to extract.
        //! \return array pointer to the pixels.
        //! Note: data is returned with full ownership.
        virtual void *ReadFramePixels(uint32_t x, uint32_t y, uint32_t width, uint32_t height) = 0;

        //! Get the dimensions of the required texture.
        //! \param texture Texture to retrieve the information about.
        //! \return Vector containing the width, height and length (respectively).
        virtual ivec3 GetTextureDimensions(Texture texture) = 0;

        //! Create a new Framebuffer with not attachments.
        //! \return Newly created framebuffer object.
        virtual void CreateFramebuffer(Framebuffer framebuffer) = 0;

        //! Attach a color texture to the framebuffer.
        //! \param framebuffer Framebuffer on which to attach the color texture.
        //! \param texture Texture on which the color data will be written.
        virtual void AttachColorTexture(Framebuffer framebuffer, Texture texture) = 0;

        //! Add multiple color textures to the framebuffer.
        //! \param framebuffer Framebuffer on which to attach the color textures.
        //! \param textures Textures on which the color data will be written.
        virtual void AttachColorTextures(Framebuffer framebuffer, vector<Texture> textures) = 0;

        //! Attach a depth texture to the framebuffer.
        //! \param framebuffer Framebuffer on which to attach the depth texture.
        //! \param texture Texture on which the depth data will be written.
        virtual void AttachDepthTexture(Framebuffer framebuffer, Texture texture) = 0;

        //! Get the color texture attached to the framebuffer.
        //! \param framebuffer Framebuffer to extract the color texture from.
        //! \return Handle to the queried Color texture if found, Invalid handle if not.
        virtual Texture GetColorTexture(Framebuffer framebuffer) = 0;

        //! Release the resources of a texture from the GPU.
        //! \param texture texture to release.
        virtual void ReleaseTexture(Texture &texture) = 0;

        //! Release the vertex buffer and all its data from the GPU.
        //! \param vertexBuffer The VertexBuffer to release.
        virtual void ReleaseVertexBuffer(VertexBuffer &vertexBuffer) = 0;

        //! Release the index buffer and all its data from the GPU.
        //! \param indexBuffer The IndexBuffer to release.
        virtual void ReleaseIndexBuffer(IndexBuffer &indexBuffer) = 0;

        /// Release a framebuffer from the Graphics API.
        /// Note: The textures attached to the framebuffer are NOT released.
        /// \param framebuffer Framebuffer to release.
        virtual void ReleaseFramebuffer(Framebuffer &framebuffer) = 0;

        /// Release the shader program and all ths shaders attached to it.
        /// \param shaderProgram Shader program to release.
        virtual void ReleaseShaderProgram(ShaderProgram &shaderProgram) = 0;

		/// Instruct the GPU to only draw the front geometry.
		virtual void EnableBackCulling() = 0;

        //! Render all the registered entries and clear the list afterwards.
        virtual void Render() = 0;

        //! Render all the registered entries and clear the list afterwards.
        //! \param framebuffer Framebuffer storing the rendering result.
        virtual void Render(Framebuffer framebuffer) = 0;

        //! Clear the Color and depth of the screen.
        virtual void ClearScreen(const Color &color) = 0;

        //! Swap the buffer of the current screen.
        virtual void SwapBuffers() = 0;

        //! Check if any error has occurred during the last frame.
        virtual void CheckErrors() const = 0;

        /// Returns the ID of the Primitive as used by the selected Graphics API.
        /// If the primitive hasn't been initialized, OGA_INVALID_ID is returned.
        /// \param primitive Primitive to get the ID for.
        /// \return The id of the primitive as assigned by the Graphics API (ex, OpenGL).
        virtual size_t GetApiIndex(Primitive primitive) const = 0;

		/// Gets the elapsed time for the last rendering pass.
		/// \return The elapsed time value (delta time).
		virtual f32 GetElapsedTime() = 0;

		/// Gets the calculated frame rates from the elapsed time.
		/// \return The frame rates.
		virtual uint16_t GetFrameRates() = 0;

        //! Set the viewport resolution.
        //! \param x Coordinate from the left of the screen in the X Axis.
        //! \param y Coordinate from the bottom of the screen in the Y Axis.
        //! \param width Width of the GL Viewport.
        //! \param height Height of the GL Viewport.
        virtual void SetViewport(uint16_t x, uint16_t y, uint16_t width, uint16_t height) = 0;

        //! Set the Color to clear the background with.
        //! \param color Color to clear the background with.
        inline void SetClearColor(const Color &color) { this->backgroundColor = color; }

        /// Called when the Renderer needs to shutdown and release its resources.
        virtual void Shutdown() = 0;
    };
}

#endif //OPENGRAPHICS_RENDERER_H_H

#ifndef OPENGRAPHICS_MEMORY_H
#define OPENGRAPHICS_MEMORY_H


#include "include.h"

namespace OGA {
    struct Memory {
        /// Contains the data of the memory chunk.
        void *data = nullptr;

        /// Size of the data to store.
        size_t size = 0;
    };

    /// The Linear allocator is used to allocate one chunk of memory,
    /// And distribute it "unequally" when allocating new object.
    /// The first allocation is somewhat big, but then it's very quick.
    /// And object allocated with this class can only be deleted when the allocator
    /// is reset or destroyed. Meaning, the user cannot Release a pointer, per say.
    /// \tparam Size Size of the allocator.
    template<uint64_t Size>
    struct LinearAllocator {
        struct Slot {
            /// Pointer to the first allocated address.
            void *start = nullptr;

            /// Offset from the first allocated pointer
            uint32_t cursor = 0;
        };

        /// The LinearAllocator has one slot only. It keeps using until it's filled.
        Slot slot;

        LinearAllocator() {
            LinearAllocator<Size>::slot.start = operator new(Size);
        }

        ~LinearAllocator() {
            free(LinearAllocator<Size>::slot.start);
        }

        /// Set the cursor the 0, and the
        inline void Reset() { LinearAllocator<Size>::slot.cursor = 0; }

        /// Allocate a new object of type T.
        /// \tparam T Type of the object allocate.
        /// \return Newly created T object. Throw exception if there isn't size to create it.
        template<typename T>
        T *Allocate() {
            const auto size = sizeof(T);
            const auto alignment = alignof(T);
            const auto delta = Size - LinearAllocator<Size>::slot.cursor;
            OGA_ASSERT(delta > size && delta > 0,
                       "LinearAllocator reached its limit, please consider extending its size");
            auto pointer = reinterpret_cast<Slot *>(reinterpret_cast<uintptr_t>(LinearAllocator<Size>::slot.start)
                                                    + LinearAllocator<Size>::slot.cursor);
            auto padding = (reinterpret_cast<uintptr_t>(pointer) - alignment) % alignment;
            LinearAllocator<Size>::slot.cursor += size + padding;
            auto object = reinterpret_cast<T *>(pointer);
            new(object) T();
            return object;
        }
    };
}

#endif //OPENGRAPHICS_MEMORY_H

//
// Created by Physiix on 20/05/17.
//

#ifndef OPENGRAPHICS_WINDOWPROVIDER_H
#define OPENGRAPHICS_WINDOWPROVIDER_H

#ifdef _WIN32
#define NOMINMAX
#define LEANANDMEAN
#define GLFW_EXPOSE_NATIVE_WIN32
#elif defined __linux__
#define GLFW_EXPOSE_NATIVE_X11


#elif defined __APPLE__
#define GLFW_EXPOSE_NATIVE_COCOA
#endif

#include "../maths/math.h"
#include "../oga_data.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

namespace OGA {
    struct WindowHandle {
        typedef void *DisplayType;
        typedef void *Handle;
        typedef void *Device;

		/// Native Display type if the application is running on X11 Server.
        DisplayType displayType = nullptr;

		/// Pointer to the native handle of the application. (X11 for Unix, WIN32 for Windows)
        Handle nativeHandle = nullptr;

		/// Direct3D*/Vulkan device or OpenGL context.
        Device device = nullptr;

		/// Top level handle to the window.
        GLFWwindow *window = nullptr;

		/// Release all the resource of the window handle.
		/// The native
        void Shutdown() const {
            glfwDestroyWindow(WindowHandle::window);
            glfwTerminate();
        }
    };

	class window_provider final {
    private:
		/// Get the native handle of the Window.
		/// \param windowHandle The window to get the native handle for.
        static void GetNativeHandle(WindowHandle &windowHandle) {
#ifdef __linux__
            windowHandle.nativeHandle = reinterpret_cast<void *>(glfwGetX11Window(windowHandle.window));
            windowHandle.displayType = reinterpret_cast<void *>(glfwGetX11Display());
#elif _WIN32
            windowHandle.nativeHandle = reinterpret_cast<void *>(glfwGetWin32Window(windowHandle.window));
#elif __APPLE__
            windowHandle.nativeHandle = reinterpret_cast<void *>(glfwGetCocoaWindow(windowHandle.window));
#endif
        }

		/// Create a window for the specified Graphics API.
		/// For now, GLFW is used for all Graphics API, but the device/context
		/// Are created differently.
		/// \param apiInfo Contains the information about the platform and the Graphics API.
		/// \param appInfo Contains the information about the application and window.
        static void SpawnWindow(WindowHandle &handle, const ApiInfo &apiInfo,
                                const ApplicationInfo &appInfo) {

            switch (apiInfo.graphicsApi) {
                case GraphicsApi::OpenGL:
                    glfwDefaultWindowHints();
					glfwWindowHint(GLFW_SAMPLES, appInfo.screenInfo.samplingCount);

                    // Render the window invisible if offscreen is enabled.
                    glfwWindowHint(GLFW_VISIBLE, !appInfo.screenInfo.offscreen);

					glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
					glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);

                    handle.window = glfwCreateWindow(appInfo.width, appInfo.height, appInfo.name.c_str(), NULL, NULL);

                    OGA_ASSERT(handle.window != nullptr, "Failed to initialize the GLFW Window");
                    glfwMakeContextCurrent(handle.window);

                    //Initializing the GLAD library
                    OGA_ASSERT(static_cast<bool>(gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)),
                               "Failed to initialize GLAD Library.");
                    break;
                case GraphicsApi::Vulkan:
                    break;
                case GraphicsApi::Direct3D9:;
                case GraphicsApi::Direct3D11:;
                case GraphicsApi::Direct3D12:
                    break;
                case GraphicsApi::DEFAULT:
                    break;
            }
        }

    public:
		/// Create a Window handle that can be used with the current Graphics API.
		/// The build logic is selected depending on the Graphics API.
		/// The OpenGL Context is only created if OpenGL is used.
		/// The Direct3D Context is only created if Direct3D is used.
		/// \param apiInfo Contains the information about the platform and the Graphics API.
		/// \param appInfo Contains the information about the application and window.
		/// \return WindowHandle object.
        static WindowHandle Provide(const ApiInfo &apiInfo, const ApplicationInfo &applicationInfo) {
            OGA_ASSERT(static_cast<bool>(glfwInit()), "Failed to initialize the GLFW Library.");
            WindowHandle handle;

            SpawnWindow(handle, apiInfo, applicationInfo);
            GetNativeHandle(handle);

            return handle;
        }
    };
}

#endif //OPENGRAPHICS_WINDOWPROVIDER_H

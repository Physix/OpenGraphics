//
// Created by Physiix on 19/05/17.
//

#ifndef OPENGRAPHICS_WINDOWS_H
#define OPENGRAPHICS_WINDOWS_H

#include "../maths/math.h"
#include "window_provider.h"
#include "../oga_data.h"
#include "inputs.h"

namespace OGA {
	class Window {
	private:
		//! Handle the Native window.
		WindowHandle windowHandle;

	public:
		//! Resize the currently available window.
		//! \param width New Width of the window.
		//! \param height New Height of the window.
		void Resize(uint32_t width, uint32_t height) {}

		//! Setup a new Window.
		//! The kind of the handler is defined by the Graphics API used.
		//! The windows is launched once initialized.
		//! \param apiInfo Contains the information about the platform and the Graphics API.
		//! \param appInfo Contains the information about the application and window.
		void Setup(const ApiInfo &apiInfo, const ApplicationInfo &appInfo) {
			Window::windowHandle = window_provider::Provide(apiInfo, appInfo);
		}

		//! Release all the resource used by the Window.
		//! The Graphics API device/context is also destroyed.
		void Shutdown() const {
			windowHandle.Shutdown();
		}

		//! Function called when a close requested is submitted by the user.
		//! \return True if a close request has been issued, false if not.
		bool IsCloseRequested() const {
			return static_cast<bool>(glfwWindowShouldClose(Window::windowHandle.window));
		}

		//! Poll all the events of the last frame.
		void PollEvents() const {
			glfwPollEvents();
		}

		//! Get the resolution of the current Window.
		//! \return The current resolution of the Window.
		ivec2 GetResolution() {
			int width, height;
			glfwGetWindowSize(Window::windowHandle.window, &width, &height);
			return OGA::ivec2(width, height);
		}

		/// Check if a keyboard key has been pressed.
		/// \param key KeyCode to check.
		/// \return True if the key has been pressed, false if not.
		bool IsKeyPressed(KeyCode key) const {
			return glfwGetKey(Window::windowHandle.window, key) == GLFW_PRESS;
		}

		/// Get the GLFW Context and returns it.
		/// \return Return the GLFW Context.
		inline GLFWwindow *GetContext() {
			return Window::windowHandle.window;
		}

		/// Read the window's dimension back.
		/// \param applicationInfo Store the width inside {applicationInfo.width}
		/// and the height inside {applicationInfo.height}.
		void GetDimensions(ApplicationInfo &applicationInfo) {
			int w = 0, h = 0;
			glfwGetWindowSize(Window::windowHandle.window, &w, &h);
			applicationInfo.width = static_cast<uint32_t>(w);
			applicationInfo.height = static_cast<uint32_t>(h);
		}
	};
}
#endif //OPENGRAPHICS_WINDOWS_H

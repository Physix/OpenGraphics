//
// Created by Physiix on 08/08/17.
//

#ifndef OPENGRAPHICS_INPUTS_H
#define OPENGRAPHICS_INPUTS_H

namespace OGA {
#define OGA_KEY_SPACE              32
#define OGA_KEY_APOSTROPHE         39  /* ' */
#define OGA_KEY_COMMA              44  /* , */
#define OGA_KEY_MINUS              45  /* - */
#define OGA_KEY_PERIOD             46  /* . */
#define OGA_KEY_SLASH              47  /* / */
#define OGA_KEY_0                  48
#define OGA_KEY_1                  49
#define OGA_KEY_2                  50
#define OGA_KEY_3                  51
#define OGA_KEY_4                  52
#define OGA_KEY_5                  53
#define OGA_KEY_6                  54
#define OGA_KEY_7                  55
#define OGA_KEY_8                  56
#define OGA_KEY_9                  57
#define OGA_KEY_SEMICOLON          59  /* ; */
#define OGA_KEY_EQUAL              61  /* = */
#define OGA_KEY_A                  65
#define OGA_KEY_B                  66
#define OGA_KEY_C                  67
#define OGA_KEY_D                  68
#define OGA_KEY_E                  69
#define OGA_KEY_F                  70
#define OGA_KEY_G                  71
#define OGA_KEY_H                  72
#define OGA_KEY_I                  73
#define OGA_KEY_J                  74
#define OGA_KEY_K                  75
#define OGA_KEY_L                  76
#define OGA_KEY_M                  77
#define OGA_KEY_N                  78
#define OGA_KEY_O                  79
#define OGA_KEY_P                  80
#define OGA_KEY_Q                  81
#define OGA_KEY_R                  82
#define OGA_KEY_S                  83
#define OGA_KEY_T                  84
#define OGA_KEY_U                  85
#define OGA_KEY_V                  86
#define OGA_KEY_W                  87
#define OGA_KEY_X                  88
#define OGA_KEY_Y                  89
#define OGA_KEY_Z                  90
#define OGA_KEY_LEFT_BRACKET       91  /* [ */
#define OGA_KEY_BACKSLASH          92  /* \ */
#define OGA_KEY_RIGHT_BRACKET      93  /* ] */
#define OGA_KEY_GRAVE_ACCENT       96  /* ` */
#define OGA_KEY_WORLD_1            161 /* non-US #1 */
#define OGA_KEY_WORLD_2            162 /* non-US #2 */

/* Function keys */
#define OGA_KEY_ESCAPE             256
#define OGA_KEY_ENTER              257
#define OGA_KEY_TAB                258
#define OGA_KEY_BACKSPACE          259
#define OGA_KEY_INSERT             260
#define OGA_KEY_DELETE             261
#define OGA_KEY_RIGHT              262
#define OGA_KEY_LEFT               263
#define OGA_KEY_DOWN               264
#define OGA_KEY_UP                 265
#define OGA_KEY_PAGE_UP            266
#define OGA_KEY_PAGE_DOWN          267
#define OGA_KEY_HOME               268
#define OGA_KEY_END                269
#define OGA_KEY_CAPS_LOCK          280
#define OGA_KEY_SCROLL_LOCK        281
#define OGA_KEY_NUM_LOCK           282
#define OGA_KEY_PRINT_SCREEN       283
#define OGA_KEY_PAUSE              284
#define OGA_KEY_F1                 290
#define OGA_KEY_F2                 291
#define OGA_KEY_F3                 292
#define OGA_KEY_F4                 293
#define OGA_KEY_F5                 294
#define OGA_KEY_F6                 295
#define OGA_KEY_F7                 296
#define OGA_KEY_F8                 297
#define OGA_KEY_F9                 298
#define OGA_KEY_F10                299
#define OGA_KEY_F11                300
#define OGA_KEY_F12                301
#define OGA_KEY_F13                302
#define OGA_KEY_F14                303
#define OGA_KEY_F15                304
#define OGA_KEY_F16                305
#define OGA_KEY_F17                306
#define OGA_KEY_F18                307
#define OGA_KEY_F19                308
#define OGA_KEY_F20                309
#define OGA_KEY_F21                310
#define OGA_KEY_F22                311
#define OGA_KEY_F23                312
#define OGA_KEY_F24                313
#define OGA_KEY_F25                314
#define OGA_KEY_KP_0               320
#define OGA_KEY_KP_1               321
#define OGA_KEY_KP_2               322
#define OGA_KEY_KP_3               323
#define OGA_KEY_KP_4               324
#define OGA_KEY_KP_5               325
#define OGA_KEY_KP_6               326
#define OGA_KEY_KP_7               327
#define OGA_KEY_KP_8               328
#define OGA_KEY_KP_9               329
#define OGA_KEY_KP_DECIMAL         330
#define OGA_KEY_KP_DIVIDE          331
#define OGA_KEY_KP_MULTIPLY        332
#define OGA_KEY_KP_SUBTRACT        333
#define OGA_KEY_KP_ADD             334
#define OGA_KEY_KP_ENTER           335
#define OGA_KEY_KP_EQUAL           336
#define OGA_KEY_LEFT_SHIFT         340
#define OGA_KEY_LEFT_CONTROL       341
#define OGA_KEY_LEFT_ALT           342
#define OGA_KEY_LEFT_SUPER         343
#define OGA_KEY_RIGHT_SHIFT        344
#define OGA_KEY_RIGHT_CONTROL      345
#define OGA_KEY_RIGHT_ALT          346
#define OGA_KEY_RIGHT_SUPER        347
#define OGA_KEY_MENU               348
    using KeyCode = int;
}

#endif //OPENGRAPHICS_INPUTS_H

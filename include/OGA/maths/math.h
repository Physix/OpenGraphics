//
// Created by Physiix on 19/05/17.
//

#ifndef OPENGRAPHICS_VEC_H
#define OPENGRAPHICS_VEC_H


#include <cmath>
#include <type_traits>
#include "../include.h"

namespace OGA {
    class Vector {
    };

    template<typename Tp, typename = typename std::enable_if<std::is_fundamental<Tp>{}, Tp>>
    struct Vector2 : Vector {
        Tp x;
        Tp y;

        Vector2(Tp x, Tp y) : x(x), y(y) {}

        Vector2() {}

        //! Create a normalized copy of the current Vector.
        //! \return a normalized copy of the current Vector.
        inline Vector2<Tp> Normalized() const {
            Vector2<Tp> vector = *this;
            vector.Normalize();
            return vector;
        }

        //! Normalize the components of the current Vector.
        //! \return Last length of the vector.
        inline Tp Normalize() {
            const auto length = Vector2<Tp>::Length();
            Vector2<Tp>::x = Vector2<Tp>::x / length;
            Vector2<Tp>::y = Vector2<Tp>::y / length;
            return length;
        }

        //! Calculate the square product of all the vector's component.
        //! \return Squared product of the vector's component
        inline Tp SquaredLength() const {
            return x * x + y * y;
        }

        //! Calculate the length of the vector.
        //! \return Length of the vector.
        inline Tp Length() const {
            return sqrt(Vector2<Tp>::SquaredLength());
        }
    };


    template<typename Tp, typename = typename std::enable_if<std::is_fundamental<Tp>{}, Tp>>
    struct Vector3 {
        Tp x;
        Tp y;
        Tp z;
    };

    template<typename Tp, typename = typename std::enable_if<std::is_fundamental<Tp>{}, Tp>>
    struct Vector4 {
        Vector4(Tp x, Tp y, Tp z, Tp w) : x(x), y(y), z(z), w(w) {}

        Vector4() {}

        Tp x;
        Tp y;
        Tp z;
        Tp w;
    };

    typedef Vector2<f32> vec2;
    typedef Vector3<f32> vec3;
    typedef Vector4<f32> vec4;
    typedef vec4 Color;


    typedef Vector2<int> ivec2;
    typedef Vector3<int> ivec3;
    typedef Vector4<int> ivec4;

    template<typename Tp, typename = typename std::enable_if<std::is_fundamental<Tp>{}, Tp>>
    struct Matrix4x4 {
        Tp m11, m12, m13, m14;
        Tp m21, m22, m23, m24;
        Tp m31, m32, m33, m34;
        Tp m41, m42, m43, m44;
    };

    typedef Matrix4x4<f32> mat4;
}



#endif //OPENGRAPHICS_VEC_H

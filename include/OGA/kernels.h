#ifndef OPENGRAPHICS_KERNELS_H_H
#define OPENGRAPHICS_KERNELS_H_H

#include <functional>
#include "include.h"


struct GLFWwindow;
namespace OGA {
    class Kernel;

    enum class RenderingStage {
        PRE_RENDERING,
        POST_RENDERING,
        BOTH
    };

    struct KernelData {
        /// Pointer to the current Kernel.
        Kernel *kernel;

        /// Handle the current Window.
        GLFWwindow *handle;

        /// Rendering stage at which the kernel is executed.
        RenderingStage stage;
    };

    using KernelCallback = std::function<void(KernelData &)>;

    /// A Kernel is a component that can be
    /// pluged in to the rendering process.
    /// It offers functions that are called at different
    /// Stage of the rendering.
    class Kernel final {
        friend class KernelManager;

    private:
        /// Unique ID of the kernel.
        const String id;

        /// Whether the kernel is finished and needs to be freed.
        bool finished = false;

        /// Version of the plugin.
        const String version;

        /// Stage at which the kernel callback function is called.
        const RenderingStage stage;

        /// Callback to execute.
        const KernelCallback callback;

        Kernel(const String &id, const String &version, const RenderingStage stage, KernelCallback callback)
                : id(id), version(version), stage(stage), callback(std::move(callback)) {}

    public:
        ~Kernel() = default;
    };


    class KernelManager {
    private:
        struct KernelHandle {
            /// Kernel to handle. Note this is not a reference, but the real kernel.
            Kernel kernel;

            /// Index of the handle.
            size_t index = 0;

            /// Contains id of the Kernel depending on.
            String dependency = "";

            /// Whether the Handle has already been sorted or not.
            bool sorted = false;

			KernelHandle(const Kernel &kernel, size_t index, const String &dependency) : kernel(kernel), index(index),
																						 dependency(dependency) {}

			KernelHandle() = default;
        };
        using Kernels = vector<KernelHandle>;

        /// Contains all the currently running kernels.
        static Kernels kernels;

    private:
        static ssize_t GetDependencyIndex(const String &dependencyId) {
            for (auto &h: KernelManager::kernels)
                if (h.kernel.id == dependencyId)
                    return h.index;
            return -1;
        }

        /// Simple Topological sort using the dependencyId and the kernels.
        /// \param kernels
        /// \param handle
        static void TopologicalSort(Kernels &kernels, KernelHandle &handle) {
            handle.sorted = true;

            // Recursive call to the topological sort if the dependent kernel also has a dependency.
            if (!handle.dependency.empty()) {
                const ssize_t depId = GetDependencyIndex(handle.dependency);
                if (depId != -1)
                    KernelManager::TopologicalSort(kernels, kernels[depId]);
            }

            // Add the kernels as all his dependencies has been added.
            handle.index = kernels.size();
            // Quick fix to having multiple instances of kernels. Since the function doesn't called often, it's OK.
            for (auto kernel: kernels)
                if (kernel.kernel.id == handle.kernel.id)
                    return;
            kernels.push_back(std::move(handle));
        }

        /// This function create a new list to stored the sorted kernels.
        /// Iterate through all the kernels and apply a topological sort
        /// to sort them by dependency.
        static void Sort() {
            Kernels orderedKernels;

            // Set the sorted flag to false for all the kernels.
            for (auto &handle: KernelManager::kernels) handle.sorted = false;

            for (auto i = 0u; i < KernelManager::kernels.size(); ++i) {
                // Skip if there is no dependency.
                if (kernels[i].sorted) continue;

                // Getting the id of the kernel dependency.
                kernels[i].sorted = true;
                const ssize_t dependency = KernelManager::GetDependencyIndex(kernels[i].dependency);
                if (dependency != -1)
                    KernelManager::TopologicalSort(orderedKernels, kernels[dependency]);
                orderedKernels.push_back(kernels[i]);
            }

            // Switch the older kernels container by the new sorted one.
            KernelManager::kernels = std::move(orderedKernels);
        }

    public:
        /// Create a new Kernel and attach it to the rendering process.
        /// \param ID Unique ID for the Kernel.
        /// \param version Version of the Kernel to create.
        /// \param stage Stage at which the Kernel is executed.
        /// \param callback Callback containing logic to execute.
        /// \param dependency ID of the Kernel depending on.
        static void
        Attach(const String &ID, const String &version, const RenderingStage stage,
               KernelCallback callback, String dependency = "") {
            // Attach the kernel.
            const auto id = KernelManager::kernels.size();
            KernelManager::kernels.push_back(
					KernelHandle{Kernel(ID, version, stage, std::move(callback)), id, dependency});

            // Sort the kernels by dependencies.
            KernelManager::Sort();
        }

        /// Iterate trough all the attached Kernels, and execute
        /// each kernel before the rendering pass.
        /// \tparam Context Type of the Context.
        /// \param context Context of the Graphics API.
        template<typename Context>
        static void PreRender(Context *context) {
            for (auto handle: KernelManager::kernels)
                if (handle.kernel.stage == RenderingStage::PRE_RENDERING ||
                    handle.kernel.stage == RenderingStage::BOTH) {
                    auto data = KernelData{&handle.kernel, (GLFWwindow *) context, RenderingStage::PRE_RENDERING};
                    handle.kernel.callback(data);
                }
        }

        /// Iterate trough all the attached Kernels, and execute
        /// each kernel after the rendering pass.
        /// \tparam Context Type of the Context.
        /// \param context Context of the Graphics API.
        template<typename Context>
        static void PostRender(Context *context) {
            for (auto handle: KernelManager::kernels)
                if (handle.kernel.stage == RenderingStage::POST_RENDERING ||
                    handle.kernel.stage == RenderingStage::BOTH) {
                    auto data = KernelData{&handle.kernel, (GLFWwindow *) context, RenderingStage::PRE_RENDERING};
                    handle.kernel.callback(data);
                }
        }
    };
}

#endif //OPENGRAPHICS_KERNELS_H_H
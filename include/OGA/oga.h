//
// Created by Physiix on 19/05/17.
//

#ifndef OPENGRAPHICS_OGA_H
#define OPENGRAPHICS_OGA_H

#include "context.h"

namespace OGA {
    void SetApiInfo(const ApiInfo &apiInfo);
    void SetApplicationInfo(const ApplicationInfo &applicationInfo);
	const ApplicationInfo &GetApplicationInfo();
    void SetScreenInfo(const ScreenInfo &screenInfo);

    //! Initialize the Graphics Api and create a new Window.
    //! \param graphicsApi Graphics API to use.
    void Init(GraphicsApi graphicsApi = GraphicsApi::DEFAULT);

    //! Start streaming data for the next data to render.
    //! \return Number of the entry.
    size_t Begin();

    //! Create a new ArrayBuffer.
    //! \return Newly created ArrayBuffer.
    ArrayBuffer CreateArrayBuffer();

    //! Create a new Vertex Buffer object.
    //! \param data Data to store inside the buffer.
    //! \param size Size of the buffer.
    //! \param container Contains the information about the vertex data to bind.
    //! \return Newly created VertexBuffer
    VertexBuffer CreateVertexBuffer(void *data, uint32_t
    size, std::initializer_list<VertexAttribute> attributes);

    //! Create a buffer to store the indices of a mesh.
    //! \param data Contains all the indices to store.
    //! \param size Size of the buffer.
    //! \return New created IndexBuffer.
    IndexBuffer CreateIndexBuffer(void *data, uint32_t size);

    //! Create a Shader Program using the vertex and fragment sources.
    //! \param vertexSource Vertex shader source code in the language of the current Graphics API.
    //! \param fragmentSource Fragment shader source code in the language of the current Graphics API.
    //! \return Newly created ShaderProgram.
    ShaderProgram CreateProgram(const String &vertexSource, const String &fragmentSource);

    //! Set the vertex buffer for the current Entry.
    //! \param vertexBuffer Vertex buffer to set.
    void SetVertexBuffer(VertexBuffer vertexBuffer);

    //! Set the index buffer for the current Entry.
    //! \param indexBuffer Vertex buffer to set.
    void SetIndexBuffer(IndexBuffer indexBuffer);

    //! Set the array buffer for the current Entry.
    //! \param arrayBuffer Vertex buffer to set.
    void SetArrayBuffer(ArrayBuffer arrayBuffer);

    //! Create a uniform that can be bind to a ShaderProgram.
    //! \param name Name of the uniform to create.
    //! \param type Type of uniform to create.
    //! \return The newly created uniform.
    Uniform CreateUniform(const String &name, UniformType type);

    //! Set a uniform value for the current Entry.
    //! \param uniform Uniform to set.
    //! \param value Value of the uniform.
    void SetUniform(Uniform uniform, const void *value);

    /// Bind a texture to the current Entry.
    /// \param texture Texture to bind.
    /// \param uniformName Name of the texture as used in the shader.
    void BindTexture(Texture texture, const String uniformName);

    //! Create a new 1D Texture handle and populate with the data.
    //! If the data is null, the texture is initialized as black image.
    //! \param data Data to initialize the texture with.
    //! \param width Size of the texture in the X Axis.
    //! \return Newly create 1D Texture.
    Texture CreateTexture1D(void *data, uint16_t width);

    //! Create a new 2D Texture handle and populate with the data.
    //! If the data is null, the texture is initialized as black image.
    //! \param data Data to initialize the texture with.
    //! \param width Size of the texture in the X Axis.
    //! \param height Size of the texture in the Y Axis.
    //! \return Newly create 2D Texture.
    Texture CreateTexture2D(void *data, uint16_t width, uint16_t height);

    //! Create a new 3D Texture handle and populate with the data.
    //! If the data is null, the texture is initialized as black image.
    //! \param data Data to initialize the texture with.
    //! \param width Size of the texture in the X Axis.
    //! \param height Size of the texture in the Y Axis.
    //! \param length Size of the texture in the Z Axis.
    //! \return Newly create 3D Texture.
    Texture CreateTexture3D(void *data, uint16_t width, uint16_t height, uint16_t length);

    //! Extract data from a texture with the Graphics Api.
    //! The data is then stored and pointer to it is returned.
    //! \param texture Texture the data from.
    //! \param data Container for the data of the texture. If null, a new container is created.
    //! \return Pointer to the texture data or nullptr.
    void *GetTextureData(Texture texture, void *data);

    //! Read the screen Pixels from the current framebuffer.
    //! \return array pointer to the pixels.
    //! Note: data is returned with full ownership.
    void *ReadFramePixels();

    //! Get the dimensions of the required texture.
    //! \param texture Texture to retrieve the information about.
    //! \return Vector containing the width, height and length (respectively).
    ivec3 GetTextureDimensions(Texture texture);

    //! Create a new Framebuffer with not attachments.
    //! \return Newly created framebuffer object.
    Framebuffer CreateFramebuffer();

    //! Attach a color texture to the framebuffer.
    //! \param framebuffer Framebuffer on which to attach the color texture.
    //! \param texture Texture on which the color data will be written.
    void AttachColorTexture(Framebuffer framebuffer, Texture texture);

    //! Add multiple color textures to the framebuffer.
    //! \param framebuffer Framebuffer on which to attach the color textures.
    //! \param textures Textures on which the color data will be written.
    void AttachColorTextures(Framebuffer framebuffer, std::initializer_list<Texture> textures);

    //! Attach a depth texture to the framebuffer.
    //! \param framebuffer Framebuffer on which to attach the depth texture.
    //! \param texture Texture on which the depth data will be written.
    void AttachDepthTexture(Framebuffer framebuffer, Texture texture);

    //! Get the color texture attached to the framebuffer.
    //! \param framebuffer Framebuffer to extract the color texture from.
    //! \return Handle to the queried Color texture if found, Invalid handle if not.
    Texture GetColorTexture(Framebuffer framebuffer);

    //! Retrieve the Offscreen framebuffer.
    //! \return Framebuffer used with the offscreen rendering.
    Framebuffer GetOffscreen();

    //! Release the resources of a texture from the GPU.
    //! \param texture texture to release.
    void ReleaseTexture(Texture &texture);

    //! Release the vertex buffer and all its data from the GPU.
    //! \param vertexBuffer The VertexBuffer to release.
    void ReleaseVertexBuffer(VertexBuffer &vertexBuffer);

    //! Release the index buffer and all its data from the GPU.
    //! \param indexBuffer The IndexBuffer to release.
    void ReleaseIndexBuffer(IndexBuffer &indexBuffer);

    /// Release a framebuffer from the Graphics API.
    /// Note: The textures attached to the framebuffer are NOT released.
    /// \param framebuffer Framebuffer to release.
    void ReleaseFramebuffer(Framebuffer &framebuffer);

    /// Release the shader program and all ths shaders attached to it.
    /// \param shaderProgram Shader program to release.
    void ReleaseShaderProgram(ShaderProgram &shaderProgram);

    /// Instruct the GPU to only draw the front geometry.
    void EnableBackCulling();

    /// Push the Entry to the rendering stream.
    /// \param program Program to be used by the Entry.
    void Push(ShaderProgram program);

    /// Push the Entry to the rendering stream, specifying the framebuffer to render to.
    /// \param program Program to be used to render the Entry.
    /// \param framebuffer Framebuffer to render the Entry into.
    void Push(ShaderProgram program, Framebuffer framebuffer);

    //! Render all the pushed entries and pass to the next frame.
    void Frame();

    //! Render all the registered entries in a framebuffer.
    //! This function doesn't pass to the next frame.
    //! \param framebuffer Framebuffer to render the scene into.
    void Frame(Framebuffer framebuffer);

    //! Function called when a close requested is submitted by the user.
    //! \return True if a close request has been issued, false if not.
    bool IsCloseRequested();

    /// Check if a keyboard key has been pressed.
    /// \param key KeyCode to check.
    /// \return True if the key has been pressed, false if not.
    bool IsKeyPressed(KeyCode code);

    /// Returns the ID of the Primitive as used by the selected Graphics API.
    /// If the primitive hasn't been initialized, OGA_INVALID_ID is returned.
    /// \param primitive Primitive to get the ID for.
    /// \return The id of the primitive as assigned by the Graphics API (ex, OpenGL).
    size_t GetApiIndex(Primitive primitive);

    /// Gets the elapsed time for the last rendering pass.
    /// \return The elapsed time value (delta time).
    f32 GetElapsedTime();

    /// Gets the calculated frame rates from the elapsed time.
    /// \return The frame rates.
    uint16_t GetFrameRates();

    //! Request a shutdown of the API.
    //! Release all the resources used by the API.
    void Shutdown();
}
#endif //OPENGRAPHICS_OGA_H

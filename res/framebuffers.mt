#Name Framebuffers
#Vertex
#Lang glsl
#version 420
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;

layout(location = 0) out vec3 vsColor;

uniform float m_Size;

void main(){
    vsColor = inColor;
    vec3 position = inPosition;
    gl_Position = vec4(position.xy * m_Size, position.z, 1);
}
#End

#Fragment
#version 420
layout(location = 0) in vec3 vsColor;
layout(location = 0) out vec4 color;

void main(){
    color = vec4(vsColor, 1);
}
#End
#version 440
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 0) out vec3 vsColor;
void main(){
    vsColor = inColor;
    vec3 position = inPosition;
    gl_Position = vec4(position, 1);
}

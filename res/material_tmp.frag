#version 440
layout(location = 0) in vec3 vsColor;
layout(location = 0) out vec4 color;
uniform float m_Time;
uniform vec4 m_Resolution;
void main(){
    if(distance(gl_FragCoord.xy, vec2(m_Resolution.x/2, m_Resolution.y/2)) > 50* sin(m_Time * 0.01))
        color = vec4(vsColor, 1);
    else
        color = vec4(1, 1, 1, 1);
}

#Name Uniforms
#Vertex
#Lang glsl
#version 420

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;

layout(location = 0) out vec3 vsColor;

void main(){
    vsColor = inColor;
    vec3 position = inPosition;
    gl_Position = vec4(position, 1);
}
#End

#Fragment
#version 420
layout(location = 0) in vec3 vsColor;
layout(location = 0) out vec4 color;

uniform vec4 m_Resolution;

void main(){
    if(distance(gl_FragCoord.xy, vec2(m_Resolution.x/2, m_Resolution.y/2)) < 100)
	    color = vec4(0,0 ,0, 1);
	else
        color = vec4(vsColor, 1);
}
#End
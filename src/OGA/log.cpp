#include "../../include/OGA/log.h"

namespace OGA {
    // Initialize the default logger.
    Log::LoggerPtr Log::logger = std::make_unique<ConsoleLog>(ConsoleLog());

    void ConsoleLog::Info(const String &message) {
        std::cout << "[Info] " << message << endl;
    }

    void ConsoleLog::Error(const String &message) {
        std::cerr << "[Error] " << message << endl;
    }

    void ConsoleLog::Warning(const String &message) {
        std::cout << "[Warning] " << message << endl;
    }
}
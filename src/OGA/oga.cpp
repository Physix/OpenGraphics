#include "../../include/OGA/oga.h"
#include "../../include/OGA/platform.h"
#include "../../include/OGA/renderer/opengl/opengl_renderer.h"
#include "../../include/OGA/rendering_pass.h"
#include "../../include/OGA/kernels.h"

namespace OGA {
	void SetApiInfo(const ApiInfo &apiInfo) {
		Context::apiInfo = apiInfo;
	}

	void SetApplicationInfo(const ApplicationInfo &applicationInfo) {
		Context::applicationInfo = applicationInfo;
	}

	const ApplicationInfo &GetApplicationInfo() {
		return Context::applicationInfo;
	}

	void SetScreenInfo(const ScreenInfo &screenInfo) {
		Context::applicationInfo.screenInfo = screenInfo;
	}

	void Init(GraphicsApi graphicsApi) {
		if (Context::initialized)
			return;

		Context::initialized = true;

		Context::apiInfo.graphicsApi = (graphicsApi == GraphicsApi::DEFAULT) ? GraphicsApi::OpenGL : graphicsApi;

		//Setting up the platform data.
		SetupPlatform();
		OGA_ASSERT(!(Context::apiInfo.platform != Platform::WINDOWS &&
					 (Context::apiInfo.graphicsApi == GraphicsApi::Direct3D9 ||
					  Context::apiInfo.graphicsApi == GraphicsApi::Direct3D11 ||
					  Context::apiInfo.graphicsApi == GraphicsApi::Direct3D12)),
				   "DirectX can only be used with the Microsoft Windows OS.");

		//Initialize the window and Graphics API.
		Context::window.Setup(Context::apiInfo, Context::applicationInfo);

		//Setting up the Renderer
		switch (graphicsApi) {
			case GraphicsApi::OpenGL:
				Context::renderer = std::make_unique<OGA::OpenGLRenderer>();
				break;
			default:
				OGA_ASSERT(false, "Renderer not supported yet.");
		}

		//Creating the global uniforms.
		Context::renderer->Init();

		// With offscreen rendering, a framebuffer is created and a color/depth textures
		// are attached.
		if (Context::applicationInfo.screenInfo.offscreen) {
			Context::mainFramebuffer = CreateFramebuffer();
			const auto width = Context::applicationInfo.width;
			const auto height = Context::applicationInfo.height;
			auto texture = OGA::CreateTexture2D(nullptr, static_cast<uint16_t>(width), static_cast<uint16_t>(height));
			auto depthTexture = OGA::CreateTexture2D(nullptr, static_cast<uint16_t>(width),
													 static_cast<uint16_t>(height));

			OGA::AttachColorTexture(Context::mainFramebuffer, texture);
			OGA::AttachDepthTexture(Context::mainFramebuffer, depthTexture);
		}
	}

	size_t Begin() {
		return RenderingPass::InitEntry();
	}

	ArrayBuffer CreateArrayBuffer() {
		OGA_ASSERT(false, "CreateArrayBuffer function is not yet supported");
		return {};
	}

	VertexBuffer CreateVertexBuffer(void *data, uint32_t size, std::initializer_list<VertexAttribute> attributes) {
		VertexBuffer vertexBuffer{};
		vertexBuffer.index = CreatePrimitiveID();
		vertexBuffer.count = Context::renderer->GetVerticesCount(size, attributes);

		auto command = RendererCommand::Create<VertexCommand>();
		command->type = RendererCommand::Type::CreateCommand;
		command->memory.data = data;
		command->memory.size = size;
		command->attributes = attributes;
		command->primitive.index = vertexBuffer.index;
		Context::renderer->commands.push_back(command);

		return vertexBuffer;
	}

	IndexBuffer CreateIndexBuffer(void *data, uint32_t size) {
		IndexBuffer indexBuffer{};
		indexBuffer.index = CreatePrimitiveID();
		indexBuffer.count = Context::renderer->GetIndicesCount(data, size);

		auto command = RendererCommand::Create<PipelineCommand>();
		command->target = RendererCommand::IndexBuffer;
		command->type = RendererCommand::Type::CreateCommand;
		command->memory.data = data;
		command->memory.size = size;
		command->primitive.index = indexBuffer.index;
		Context::renderer->commands.push_back(command);

		return indexBuffer;
	}

	ShaderProgram CreateProgram(const String &vertexSource, const String &fragmentSource) {
		ShaderProgram program{};
		program.index = CreatePrimitiveID();

		auto command = RendererCommand::Create<ShaderProgramCommand>();
		command->type = RendererCommand::Type::CreateCommand;
		command->primitive.index = program.index;
		command->vertexSource = String(vertexSource);
		command->fragmentSource = fragmentSource;
		Context::renderer->commands.push_back(command);

		return program;
	}

	void SetVertexBuffer(VertexBuffer vertexBuffer) {
		RenderingPass::current.vertexBuffer = vertexBuffer;
	}

	void SetIndexBuffer(IndexBuffer indexBuffer) {
		RenderingPass::current.indexBuffer = indexBuffer;
	}

	void SetArrayBuffer(ArrayBuffer arrayBuffer) {
		RenderingPass::current.arrayBuffers.push_back(arrayBuffer);
	}

	Uniform CreateUniform(const String &name, UniformType type) {
		return Context::renderer->CreateUniform(name, type);
	}

	void SetUniform(Uniform uniform, const void *value) {
		RenderingPass::current.uniforms.emplace_back(uniform, value);
	}

	void BindTexture(Texture texture, const String uniformName) {
		RenderingPass::current.textures.emplace_back(uniformName, texture);
	}

	Texture CreateTexture1D(void *data, uint16_t width) {
		Texture texture{};
		texture.index = CreatePrimitiveID();

		auto command = RendererCommand::Create<TextureCommand>();
		command->type = RendererCommand::Type::CreateCommand;
		command->primitive.index = texture.index;
		command->width = width;
		command->textureType = TextureCommand::TextureType::TEXTURE1D;
		command->memory.data = data;
		Context::renderer->commands.push_back(command);

		return texture;
	}

	Texture CreateTexture2D(void *data, uint16_t width, uint16_t height) {
		Texture texture{};
		texture.index = CreatePrimitiveID();

		auto command = RendererCommand::Create<TextureCommand>();
		command->type = RendererCommand::Type::CreateCommand;
		command->primitive.index = texture.index;
		command->width = width;
		command->height = height;
		command->textureType = TextureCommand::TextureType::TEXTURE2D;
		command->memory.data = data;
		Context::renderer->commands.push_back(command);

		return texture;
	}

	Texture CreateTexture3D(void *data, uint16_t width, uint16_t height, uint16_t length) {
		Texture texture{};
		texture.index = CreatePrimitiveID();

		auto command = RendererCommand::Create<TextureCommand>();
		command->textureType = TextureCommand::TextureType::TEXTURE3D;
		command->primitive.index = texture.index;
		command->width = width;
		command->height = height;
		command->length = length;
		command->textureType = TextureCommand::TextureType::TEXTURE2D;
		command->memory.data = data;
		Context::renderer->commands.push_back(command);

		return texture;
	}

	void *GetTextureData(Texture texture, void *data) {
		return Context::renderer->GetTextureData(texture, data);
	}

	void *ReadFramePixels() {
		return Context::renderer->ReadFramePixels(0, 0, Context::applicationInfo.width,
												  Context::applicationInfo.height);
	}

	ivec3 GetTextureDimensions(Texture texture) {
		return Context::renderer->GetTextureDimensions(texture);
	}

	Framebuffer CreateFramebuffer() {
		Framebuffer framebuffer{};
		framebuffer.index = CreatePrimitiveID();
		auto command = RendererCommand::Create<FramebufferCommand>();
		command->primitive.index = framebuffer.index;
		command->type = RendererCommand::Type::CreateCommand;
		Context::renderer->commands.push_back(command);
		return framebuffer;
	}

	void AttachColorTexture(Framebuffer framebuffer, Texture texture) {
		auto command = RendererCommand::Create<FramebufferCommand>();
		command->primitive.index = framebuffer.index;
		command->type = RendererCommand::Type::UpdateCommand;
		command->textures.push_back(texture);
		command->attachmentType = FramebufferCommand::AttachmentType::COLOR;
		Context::renderer->commands.push_back(command);
	}

	void AttachColorTextures(Framebuffer framebuffer, std::initializer_list<Texture> textures) {
		Context::renderer->AttachColorTextures(framebuffer, textures);
	}

	void AttachDepthTexture(Framebuffer framebuffer, Texture texture) {
		auto command = RendererCommand::Create<FramebufferCommand>();
		command->primitive.index = framebuffer.index;
		command->type = RendererCommand::Type::UpdateCommand;
		command->textures.push_back(texture);
		command->attachmentType = FramebufferCommand::AttachmentType::DEPTH;
		Context::renderer->commands.push_back(command);
	}

	Texture GetColorTexture(Framebuffer framebuffer) {
		return Context::renderer->GetColorTexture(framebuffer);
	}

	Framebuffer GetOffscreen() {
		return Context::mainFramebuffer;
	}

	void ReleaseTexture(Texture &texture) {
		auto command = RendererCommand::Create<RendererCommand>();
		command->primitive.index = texture.index;
		command->target = RendererCommand::Texture;
		command->type = RendererCommand::Type::ReleaseCommand;
		Context::renderer->commands.push_back(command);
	}

	void ReleaseVertexBuffer(VertexBuffer &vertexBuffer) {
		auto command = RendererCommand::Create<RendererCommand>();
		command->primitive.index = vertexBuffer.index;
		command->target = RendererCommand::VertexBufer;
		command->type = RendererCommand::Type::ReleaseCommand;
		Context::renderer->commands.push_back(command);
	}

	void ReleaseIndexBuffer(IndexBuffer &indexBuffer) {
		auto command = RendererCommand::Create<RendererCommand>();
		command->primitive.index = indexBuffer.index;
		command->target = RendererCommand::IndexBuffer;
		command->type = RendererCommand::Type::ReleaseCommand;
		Context::renderer->commands.push_back(command);
	}

	void ReleaseFramebuffer(Framebuffer &framebuffer) {
		auto command = RendererCommand::Create<RendererCommand>();
		command->primitive.index = framebuffer.index;
		command->target = RendererCommand::Framebuffer;
		command->type = RendererCommand::Type::ReleaseCommand;
		Context::renderer->commands.push_back(command);
	}

	void ReleaseShaderProgram(ShaderProgram &shaderProgram) {
		auto command = RendererCommand::Create<RendererCommand>();
		command->primitive.index = shaderProgram.index;
		command->target = RendererCommand::ShaderProgram;
		command->type = RendererCommand::Type::ReleaseCommand;
		Context::renderer->commands.push_back(command);
	}

	void EnableBackCulling() {
		Context::renderer->EnableBackCulling();
	}

	void Push(ShaderProgram program) {
		RenderingPass::current.program = program;
		RenderingPass::Push();
	}

	void Push(ShaderProgram program, Framebuffer framebuffer) {
		RenderingPass::current.program = program;
		RenderingPass::current.framebuffer = framebuffer;
		RenderingPass::Push();
	}

	void Frame() {
		// Update the width and height values if necessary.
		Context::window.GetDimensions(Context::applicationInfo);

		// Swap the buffers and update the events.
		Context::renderer->SwapBuffers();
		Context::window.PollEvents();

		// Check if offscreen rendering is enabled, and render the
		// scene into a framebuffer if so.
		if (Context::applicationInfo.screenInfo.offscreen) {
			Frame(Context::mainFramebuffer);
			return;
		}

		// Push the entries form the rendering pass to the Renderer.
		RenderingPass::Flush(Context::renderer->renderData.entries);

		// Execute all the kernels before the rendering pass.
		KernelManager::PreRender(Context::window.GetContext());

		// Clear the screen
		const auto &color = Context::applicationInfo.screenInfo.background;
		Context::renderer->ClearScreen(color);

		// Setting up the correct window resolution.
		Context::renderer->SetViewport(0, 0, static_cast<uint16_t>(Context::applicationInfo.width),
									   static_cast<uint16_t>(Context::applicationInfo.height));

		Context::renderer->Render();

		// Execute all the kernels before the rendering pass.
		KernelManager::PostRender(Context::window.GetContext());
	}

	void Frame(Framebuffer framebuffer) {
		// Update the width and height values if necessary.
		Context::window.GetDimensions(Context::applicationInfo);

		// Push the entries form the rendering pass to the Renderer.
		RenderingPass::Flush(Context::renderer->renderData.entries);

		// Render the scene.
		const auto &color = Context::applicationInfo.screenInfo.background;
		Context::renderer->SetClearColor(color);
		Context::renderer->Render(framebuffer);
	}

	bool IsCloseRequested() {
		return Context::window.IsCloseRequested();
	}

	bool IsKeyPressed(KeyCode code) {
		return Context::window.IsKeyPressed(code);
	}

	size_t GetApiIndex(Primitive primitive) {
		return Context::renderer->GetApiIndex(primitive);
	}

	f32 GetElapsedTime() {
		return Context::renderer->GetElapsedTime();
	}

	uint16_t GetFrameRates() {
		return Context::renderer->GetFrameRates();
	}

	void Shutdown() {
		Context::renderer->Shutdown();
		Context::window.Shutdown();
	}
}

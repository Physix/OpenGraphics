#include "../../include/OGA/context.h"

namespace OGA {
	Window Context::window{};
	ApiInfo Context::apiInfo{};
	Framebuffer Context::mainFramebuffer{};
	ApplicationInfo Context::applicationInfo{};
	unique_ptr<IRenderer> Context::renderer;
	bool Context::initialized = false;
}
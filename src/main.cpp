#include "../include/OGA/oga.h"
#include "../include/OGA/shaders/compiler.h"
#include "../include/OGA/kernels.h"


using OGA::String;
using OGA::VertexBuffer;
using OGA::IndexBuffer;
using OGA::VertexAttribute;
using OGA::f32;
using OGA::ShaderProgram;
using OGA::Uniform;
using OGA::UniformType;

ShaderProgram CreateProgram(const String &vertexLocation, const String &fragLocation) {
	std::ifstream vFile(vertexLocation);
	String vertexContent;
	for (String line; getline(vFile, line);)
		vertexContent += line + "\n";
	vFile.close();

	std::ifstream fFile(fragLocation);
	String fragContent;
	for (String line; getline(fFile, line);)
		fragContent += line + "\n";
	fFile.close();

	ShaderProgram program = OGA::CreateProgram(vertexContent, fragContent);
	return program;
}

int main() {
	OGA::ApplicationInfo applicationInfo;
	applicationInfo.name = "Engine";
	applicationInfo.width = 1200;
	applicationInfo.height = 900;
	applicationInfo.screenInfo.vsync = true;

	OGA::Log::Info(applicationInfo.structureInfo.pathMaterial);
	applicationInfo.screenInfo.background = OGA::Color(0.2f, 0.2f, 0.6f, 1.f);

	OGA::SetApplicationInfo(applicationInfo);

	OGA::Init(OGA::GraphicsApi::OpenGL);

	auto compilationInfos = OGA::ParseMaterial("triangle.mt");
	compilationInfos[0].output = OGA::ShaderType::GLSL;
	compilationInfos[1].output = OGA::ShaderType::GLSL;

	auto ci = OGA::ShaderInfo();
	ci.data.path = applicationInfo.structureInfo.pathGLSL + "test1.frag";
	ci.input = OGA::ShaderType::GLSL;
	ci.output = OGA::ShaderType::GLSL;
	ci.outputVersion = 40;

	static const f32 vertices[] = {-0.5f, -0.5f, 1.f, 0.4f, 0.2f, 0.1f, 0.0f, 0.5f, 1.f, 0.8f, 0.2f, 0.2f, 0.5f, -0.5f,
								   1.f, 0.5f, 0.8f, 0.1f};

	static const uint32_t indices[] = {0, 1, 2};

	VertexBuffer vertexBuffer = OGA::CreateVertexBuffer((void *) vertices, sizeof(f32) * 18,
														{VertexAttribute(3, VertexAttribute::AttrType::Float, false),
														 VertexAttribute(3, VertexAttribute::AttrType::Float, false)});

	IndexBuffer indexBuffer = OGA::CreateIndexBuffer((void *) indices, sizeof(uint32_t) * 3);

	OGA::Log::Warning((OGA::ShaderCompiler::Compile(compilationInfos[0])).source);
	auto vertexLocation = (OGA::ShaderCompiler::Compile(compilationInfos[0])).source;
	auto fragLocation = (OGA::ShaderCompiler::Compile(compilationInfos[1])).source;
	OGA::Log::Info(vertexLocation);
	OGA::Log::Info(fragLocation);

	auto framebuffer = OGA::CreateFramebuffer();
	auto texture = OGA::CreateTexture2D(nullptr, static_cast<uint16_t>(applicationInfo.width),
										static_cast<uint16_t>(applicationInfo.height));
	OGA::AttachColorTexture(framebuffer, texture);

	ShaderProgram program = CreateProgram(vertexLocation, fragLocation);

	Uniform uniform = OGA::CreateUniform("m_Time", UniformType::Float);
	Uniform uResolution = OGA::CreateUniform("m_Resolution", UniformType::Vector4);

	OGA::KernelManager::Attach("TestKernel", "1.0", OGA::RenderingStage::POST_RENDERING, [](OGA::KernelData &data) {
		switch (data.stage) {
			case OGA::RenderingStage::PRE_RENDERING:
				break;
			case OGA::RenderingStage::POST_RENDERING:
				break;
			default:
				break;
		}
	});


	auto time = 1.f;
	while (!OGA::IsCloseRequested() && !OGA::IsKeyPressed(OGA_KEY_ESCAPE)) {
		const auto &appInfo = OGA::GetApplicationInfo();
		time += 0.1f;
		OGA::Log::Info(std::to_string(time));
		OGA::Begin();
		OGA::SetVertexBuffer(vertexBuffer);
		OGA::SetIndexBuffer(indexBuffer);
		{
//			uint32_t value = ;
			OGA::SetUniform(uniform, &time); // Value is copied.
		}

		OGA::Push(program);

		OGA::Frame(framebuffer);

		OGA::Begin();
		OGA::SetVertexBuffer(vertexBuffer);
		OGA::SetIndexBuffer(indexBuffer);
		{
			OGA::SetUniform(uniform, &time); // Value is copied.
			auto res = OGA::vec4(static_cast<f32>(appInfo.width), static_cast<f32>(appInfo.height), 0.f, 0.f);
			OGA::SetUniform(uResolution, &res);
		}

		OGA::Push(program);

		OGA::Frame();
	}
	OGA::ReleaseVertexBuffer(vertexBuffer);
	OGA::ReleaseIndexBuffer(indexBuffer);

	OGA::Shutdown();
	return 0;
}

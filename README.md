OpenGraphicsApi
===============
Simple Graphics API to abstract OpenGL/Vulkan and DirectX form the user. The rendering process is handled by `Queues`, which make it easy for the user to run the rendering process in its own thread (multi-threading support). You can see a simple multi-threading rendering example [here].


How To Use
===============
### Windows
##### Using binaries <a name="win_bin"></a>
1. Download the [binaries]
2. Link against OpenGraphicsApi.dll
3. include the `include/OGA` directory

#### From source
This project uses [cmake].

1. Download the [source]
2. Create a `build`
3. Launch cmake to generate an `sln` project.
4. Open the generated project in Microsoft Visual Studio
5. Compile the source.
6. Go to step `2` from [here](#win_bin)

### Linux/OS X
#### From source
```
git clone ... --recursive
cd OpenGraphics/external/glad
./setup.sh #Or python3 build.py
cmake -G "Unix Makefiles" ..
```

How does it work
================
